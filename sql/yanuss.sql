-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2018 a las 15:38:22
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yanuss`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activo`
--

CREATE TABLE `activo` (
  `id` int(11) NOT NULL,
  `tipo_activo_id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `activo`
--

INSERT INTO `activo` (`id`, `tipo_activo_id`, `nombre`, `descripcion`) VALUES
(2, 1, 'IMPRESORA', 'Impresora Multifunción del área de Contabilidad'),
(3, 1, 'CAMARA IP', 'Cámara de 3 mpxs tipo domo de la parte de las gradas'),
(4, 3, 'ANTIVIRUS', 'instalador antivirus version 3'),
(5, 5, 'NOTEBOOK', 'Notebook personal para control de la entrada y salida de trabajadores'),
(6, 4, 'MODEM', 'Equipo para proporcionar internet via wi-fi en el departamento de compras públicas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activo_vulnerabilidad`
--

CREATE TABLE `activo_vulnerabilidad` (
  `activo_id` int(11) NOT NULL,
  `vulnerabilidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amenaza`
--

CREATE TABLE `amenaza` (
  `id` int(11) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `integridad` decimal(10,2) DEFAULT NULL,
  `confidencialidad` decimal(10,2) DEFAULT NULL,
  `disponibilidad` decimal(10,2) DEFAULT NULL,
  `impacto` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `amenaza`
--

INSERT INTO `amenaza` (`id`, `codigo`, `nombre`, `integridad`, `confidencialidad`, `disponibilidad`, `impacto`) VALUES
(1, 'AM01', 'Incumplimiento de la mantenibilidad del sistema de información', '23.00', '24.00', '45.00', '30.67'),
(2, 'AM02', 'Destrucción de equipos o medios de comunicación', '34.00', '55.00', '55.00', '48.00'),
(3, 'AM03', 'Polvo, corrosión, congelación', '23.00', '43.00', '44.00', '36.67'),
(4, 'AM04', 'Radiación electromagnética', '34.00', '23.00', '56.00', '37.67'),
(5, 'AM05', 'Error de uso', '22.00', '54.00', '55.00', '43.67'),
(6, 'AM06', 'Pérdida de alimentación', '38.00', '56.00', '66.00', '53.33'),
(7, 'AM07', 'Fenomeno meteorologico', '21.00', '35.00', '56.00', '37.33'),
(8, 'AM08', 'Robo de medios o documentos', '33.00', '55.00', '66.00', '51.33'),
(9, 'AM09', 'Abuso de derechos', '40.00', '50.00', '30.00', '40.00'),
(10, 'AM10', 'Corrupción de datos', '23.00', '56.00', '77.00', '52.00'),
(11, 'AM11', 'Forja de derechos', '12.00', '10.00', '67.00', '29.67'),
(12, 'AM12', 'Procesamiento ilegal de datos', '64.00', '86.00', '76.00', '75.33'),
(13, 'AM013', 'Mal funcionamiento del software', '35.00', '44.00', '44.00', '41.00'),
(14, 'AM014', 'Manipulación de software', '22.00', '54.00', '32.00', '36.00'),
(15, 'AM015', 'Uso no autorizado de equipos', '54.00', '33.00', '67.00', '51.33'),
(16, 'AM016', 'Negacion de acciones', '35.00', '45.00', '44.00', '41.33'),
(17, 'AM017', 'Escuchas', '30.00', '43.00', '54.00', '42.33'),
(18, 'AM018', 'Fallo de los equipos de telecomunicación', '33.00', '44.00', '5.00', '27.33'),
(19, 'AM019', 'Espionaje remoto', '44.00', '23.00', '22.00', '29.67'),
(20, 'AM020', 'Saturación del sistema de información', '34.00', '22.00', '96.00', '50.67'),
(21, 'AM021', 'Incumplimiento de disponibilidad de personal', '23.00', '23.00', '22.00', '22.67'),
(22, 'AM022', 'Inundar', '32.00', '55.00', '45.00', '44.00'),
(23, 'AM023', 'Robo de equipo', '44.00', '49.00', '23.00', '38.67'),
(24, 'AM024', 'Datos de fuentes no confiables', '21.00', '11.00', '18.00', '16.67'),
(25, 'AM025', 'Falla en el equipo', '23.00', '34.00', '29.00', '28.67'),
(26, 'AM026', 'Uso de software falsificado o copiado', '21.00', '47.00', '69.00', '45.67');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_menu`
--

CREATE TABLE `categoria_menu` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `icono` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria_menu`
--

INSERT INTO `categoria_menu` (`id`, `codigo`, `nombre`, `icono`) VALUES
(1, 'administracion', 'Administración', 'fa big-icon fa-edit'),
(2, 'analisisRiesgo', 'Análisis Riesgo', 'fa big-icon fa-desktop');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `categoria_menu_id` int(11) NOT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT NULL,
  `icono` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `categoria_menu_id`, `codigo`, `nombre`, `ruta`, `icono`) VALUES
(1, 1, 'categoriasMenus', 'Categorías Menús', 'CategoriasMenus/index', 'fa fa-circle-o'),
(2, 1, 'menus', 'Menús', 'Menus/index', 'fa fa-circle-o'),
(3, 2, 'activos', 'Activos', 'Activos/index', 'fa fa-circle-o'),
(4, 2, 'tipoActivo', 'Tipo Activo', 'TiposActivos/index', 'fa fa-circle-o'),
(5, 2, 'vulnerabilidades', 'Vulnerabilidades', 'Vulnerabilidades/index', 'fa fa-circle-o'),
(6, 2, 'amenazas', 'Amenazas', 'Amenazas/index', 'fa fa-circle-o');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `skin` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_menu`
--

CREATE TABLE `perfil_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `perfil_id` int(11) NOT NULL,
  `lectura` varchar(5) DEFAULT NULL,
  `escritura` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_activo`
--

CREATE TABLE `tipo_activo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_activo`
--

INSERT INTO `tipo_activo` (`id`, `nombre`) VALUES
(1, 'HARDWARE'),
(3, 'SOFTWARE'),
(4, 'RED'),
(5, 'PERSONAL'),
(6, 'COMERCIO'),
(7, 'SITIO'),
(8, 'ORGANIZACION');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `clave` varchar(200) DEFAULT NULL,
  `tipo_identificacion` enum('C','P') DEFAULT 'C',
  `numero_identificacion` varchar(15) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `estado` enum('A','I') DEFAULT 'A',
  `es_superadmin` enum('N','S') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `fecha_nacimiento`, `email`, `usuario`, `clave`, `tipo_identificacion`, `numero_identificacion`, `foto`, `estado`, `es_superadmin`) VALUES
(1, 'WEBMASTER', 'USER', '1984-01-01', 'webmaster@mail.com', 'webmaster', '54e4dde95ad582ac5e60e7fa6aff1d2b204044bab8328f929942450a77d00d7bbec955a870af90519703d3e599b330425d9296f6ea9b6728b68abd8cd1f89802JfMrcU5W6wXzhUuvk3nqg/IGa/E8m0qeuRUIzvw9RaQ=', 'C', '9999999999', NULL, 'A', 'S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_perfil`
--

CREATE TABLE `usuario_perfil` (
  `usuario_id` int(11) NOT NULL,
  `perfil_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vulnerabilidad`
--

CREATE TABLE `vulnerabilidad` (
  `id` int(11) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `nombre` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vulnerabilidad`
--

INSERT INTO `vulnerabilidad` (`id`, `codigo`, `nombre`) VALUES
(2, 'VUL01', 'Mantenimiento insuficiente / instalación defectuosa de medios de almacenamiento'),
(3, 'VUL02', 'Falta de esquemas de reemplazo periódicos'),
(4, 'VUL03', 'Susceptibilidad a la humedad, polvo, suciedad'),
(5, 'VUL04', 'Sensibilidad a la radiación electromagnética'),
(6, 'VUL05', 'Falta de control de cambio de configuración eficiente'),
(7, 'VUL06', 'Susceptibilidad a las variaciones de tensión'),
(8, 'VUL07', 'Susceptibilidad a las variaciones de temperatura'),
(9, 'VUL08', 'Almacenamiento no protegido'),
(10, 'VUL09', 'Falta de cuidados a eliminacion desechos'),
(11, 'VUL10', 'Copia incontrolada'),
(12, 'VUL11', 'No posee mecanismo de respaldo'),
(13, 'VUL12', 'No posee alta disponibilidad'),
(14, 'VUL13', 'Falta de mecanismo de redundancia'),
(15, 'VUL14', 'Mantenimiento insuficiente / instalación defectuosa'),
(16, 'VUL15', 'Falta de precaucion en las conexiones fisicas'),
(17, 'VUL16', 'Falta de esquema de ampliar capacidad'),
(18, 'VUL17', 'Falla de equipos dependientes'),
(19, 'VUL18', 'Fallo de los componentes '),
(20, 'VUL19', 'Pruebas de software inexistentes o insuficientes'),
(21, 'VUL20', 'Errores conocidos en el software'),
(22, 'VUL21', 'No \'logout\' al salir de la estación de trabajo'),
(23, 'VUL22', 'Eliminación o reutilización de medios de almacenamiento sin el correcto borrado'),
(24, 'VUL23', 'Falta de pista de auditoria'),
(25, 'VUL24', 'Asignación errónea de derechos de acceso'),
(26, 'VUL25', 'Software de amplia distribución'),
(27, 'VUL26', 'Aplicando programas de aplicación a los datos incorrectos en términos de tiempo'),
(28, 'VUL27', 'Interfaz de usuario complicada'),
(29, 'VUL28', 'Falta de documentacion técnica'),
(30, 'VUL29', 'Configuración incorrecta de parámetros'),
(31, 'VUL30', 'Fechas incorrectas'),
(32, 'VUL31', 'Falta de mecanismos de identificación y autenticación como la autenticación de usuarios'),
(33, 'VUL32', 'Tablas de contraseñas no protegidas'),
(34, 'VUL33', 'Mala gestión de contraseñas'),
(35, 'VUL34', 'Servicios innecesarios habilitados'),
(36, 'VUL35', 'Software inmaduro o nuevo'),
(37, 'VUL36', 'Especificaciones poco claras o incompletas para desarrolladores'),
(38, 'VUL37', 'Falta de control efectivo del cambio'),
(39, 'VUL38', 'Descarga incontrolada y uso de software'),
(40, 'VUL39', 'Falta de copias de respaldo'),
(41, 'VUL40', 'Falta de protección física del edificio, puertas y ventanas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vulnerabilidad_amenaza`
--

CREATE TABLE `vulnerabilidad_amenaza` (
  `vulnerabilidad_id` int(11) NOT NULL,
  `amenaza_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activo`
--
ALTER TABLE `activo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activo_tipo_activo1_idx` (`tipo_activo_id`);

--
-- Indices de la tabla `activo_vulnerabilidad`
--
ALTER TABLE `activo_vulnerabilidad`
  ADD PRIMARY KEY (`activo_id`,`vulnerabilidad_id`),
  ADD KEY `fk_activo_has_vulnerabilidad_vulnerabilidad1_idx` (`vulnerabilidad_id`),
  ADD KEY `fk_activo_has_vulnerabilidad_activo1_idx` (`activo_id`);

--
-- Indices de la tabla `amenaza`
--
ALTER TABLE `amenaza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_menu`
--
ALTER TABLE `categoria_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu_categoria_menu_idx` (`categoria_menu_id`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perfil_menu`
--
ALTER TABLE `perfil_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_perfil_menu_menu1_idx` (`menu_id`),
  ADD KEY `fk_perfil_menu_perfil1_idx` (`perfil_id`);

--
-- Indices de la tabla `tipo_activo`
--
ALTER TABLE `tipo_activo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_perfil`
--
ALTER TABLE `usuario_perfil`
  ADD PRIMARY KEY (`usuario_id`,`perfil_id`),
  ADD KEY `fk_usuario_has_perfil_perfil1_idx` (`perfil_id`),
  ADD KEY `fk_usuario_has_perfil_usuario1_idx` (`usuario_id`);

--
-- Indices de la tabla `vulnerabilidad`
--
ALTER TABLE `vulnerabilidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vulnerabilidad_amenaza`
--
ALTER TABLE `vulnerabilidad_amenaza`
  ADD PRIMARY KEY (`vulnerabilidad_id`,`amenaza_id`),
  ADD KEY `fk_vulnerabilidad_has_amenaza_amenaza1_idx` (`amenaza_id`),
  ADD KEY `fk_vulnerabilidad_has_amenaza_vulnerabilidad1_idx` (`vulnerabilidad_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activo`
--
ALTER TABLE `activo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `amenaza`
--
ALTER TABLE `amenaza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `categoria_menu`
--
ALTER TABLE `categoria_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfil_menu`
--
ALTER TABLE `perfil_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_activo`
--
ALTER TABLE `tipo_activo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `vulnerabilidad`
--
ALTER TABLE `vulnerabilidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activo`
--
ALTER TABLE `activo`
  ADD CONSTRAINT `fk_activo_tipo_activo1` FOREIGN KEY (`tipo_activo_id`) REFERENCES `tipo_activo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `activo_vulnerabilidad`
--
ALTER TABLE `activo_vulnerabilidad`
  ADD CONSTRAINT `fk_activo_has_vulnerabilidad_activo1` FOREIGN KEY (`activo_id`) REFERENCES `activo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activo_has_vulnerabilidad_vulnerabilidad1` FOREIGN KEY (`vulnerabilidad_id`) REFERENCES `vulnerabilidad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_menu_categoria_menu` FOREIGN KEY (`categoria_menu_id`) REFERENCES `categoria_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfil_menu`
--
ALTER TABLE `perfil_menu`
  ADD CONSTRAINT `fk_perfil_menu_menu1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_perfil_menu_perfil1` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_perfil`
--
ALTER TABLE `usuario_perfil`
  ADD CONSTRAINT `fk_usuario_has_perfil_perfil1` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_perfil_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vulnerabilidad_amenaza`
--
ALTER TABLE `vulnerabilidad_amenaza`
  ADD CONSTRAINT `fk_vulnerabilidad_has_amenaza_amenaza1` FOREIGN KEY (`amenaza_id`) REFERENCES `amenaza` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vulnerabilidad_has_amenaza_vulnerabilidad1` FOREIGN KEY (`vulnerabilidad_id`) REFERENCES `vulnerabilidad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;