<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";

    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones



    class Pdf extends FPDF {

        var $angulo=0;

        function Rotar($angulo,$x=-1,$y=-1){
          if($x==-1)
            $x=$this->x;
          if($y==-1)
            $y=$this->y;
          if($this->angulo!=0)
            $this->_out('Q');
            $this->angulo=$angulo;
          if($angulo!=0)
            {
            $angulo*=M_PI/180;
            $c=cos($angulo);
            $s=sin($angulo);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
            }
        }

        var $B = 0;
        var $I = 0;
        var $U = 0;
        var $HREF = '';

        function WriteHTML($html)
        {
            // Intérprete de HTML
            $html = str_replace("\n",' ',$html);
            $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
            foreach($a as $i=>$e)
            {
                if($i%2==0)
                {
                    // Text
                    if($this->HREF)
                        $this->PutLink($this->HREF,$e);
                    else
                        $this->Write(5,$e);
                }
                else
                {
                    // Etiqueta
                    if($e[0]=='/')
                        $this->CloseTag(strtoupper(substr($e,1)));
                    else
                    {
                        // Extraer atributos
                        $a2 = explode(' ',$e);
                        $tag = strtoupper(array_shift($a2));
                        $attr = array();
                        foreach($a2 as $v)
                        {
                            if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                                $attr[strtoupper($a3[1])] = $a3[2];
                        }
                        $this->OpenTag($tag,$attr);
                    }
                }
            }
        }

        function OpenTag($tag, $attr)
        {
            // Etiqueta de apertura
            if($tag=='B' || $tag=='I' || $tag=='U')
                $this->SetStyle($tag,true);
            if($tag=='A')
                $this->HREF = $attr['HREF'];
            if($tag=='BR')
                $this->Ln(5);
        }

        function CloseTag($tag)
        {
            // Etiqueta de cierre
            if($tag=='B' || $tag=='I' || $tag=='U')
                $this->SetStyle($tag,false);
            if($tag=='A')
                $this->HREF = '';
        }

        function SetStyle($tag, $enable)
        {
            // Modificar estilo y escoger la fuente correspondiente
            $this->$tag += ($enable ? 1 : -1);
            $style = '';
            foreach(array('B', 'I', 'U') as $s)
            {
                if($this->$s>0)
                    $style .= $s;
            }
            $this->SetFont('',$style);
        }

        function PutLink($URL, $txt)
        {
            // Escribir un hiper-enlace
            $this->SetTextColor(0,0,255);
            $this->SetStyle('U',true);
            $this->Write(5,$txt,$URL);
            $this->SetStyle('U',false);
            $this->SetTextColor(0);
        }        

        function _endpage(){
          if($this->angulo!=0){
            $this->angulo=0;
            $this->_out('Q');
          }
          parent::_endpage();
        }


          public function __construct() {
            parent::__construct();
        }
        // El encabezado del PDF
        public function Header(){
            //$this->Image('imagenes/logo.png',10,8,22);
            //$this->load->model('institucion');
            //$datosInstitucion = datosInstitucion();
            //$this->SetFont('Arial','B',13);
            //$this->Cell(30);
            //$this->Cell(120,10,$data,0,0,'C');
            //$this->Ln('5');
            //$this->SetFont('Arial','B',8);
            //$this->Cell(30);
            //$this->Cell(120,10,'INFORMACION DE CONTACTO',0,0,'C');
            //$this->Ln(20);
       }
       // El pie del pdf
       public function Footer(){
           //$this->SetY(-15);
           //$this->SetFont('Arial','I',8);
           //$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'C');
      }


        
    }
?>