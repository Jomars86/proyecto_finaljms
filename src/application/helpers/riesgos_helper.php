<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	#login de usuario conectado
	/*
	Aut: Marcelo Quimbita
	Descripción: Verificar Login
	*/
	function tabularAmenazas(){
			$Amenazas =& get_instance();
			$Amenazas->load->model('Amenazas/Amenaza');
			$registros = $Amenazas->Amenaza->buscarAmenazaTabular();
			$registros = json_decode(json_encode($registros), true);

			$aux = array();
			$final = array();
			foreach ($registros as $arr)
			    if (!in_array($arr['nombreAmenaza'], $aux)){
			        $aux[] = $arr['nombreAmenaza'];   
			        $final[] = $arr;
			    }
			    $final = json_decode (json_encode ($final), FALSE);
			return $final;
	}


	function tabularVulnerabilidades(){
			$Vulnerabilidades =& get_instance();
			$Vulnerabilidades->load->model('Vulnerabilidades/Vulnerabilidad');
			$registros = $Vulnerabilidades->Vulnerabilidad->buscarVulnerabilidadTabular();
			$registros = json_decode(json_encode($registros), true);

			$aux = array();
			$final = array();
			foreach ($registros as $arr)
			    if (!in_array($arr['nombreVulnerabilidad'], $aux)){
			        $aux[] = $arr['nombreVulnerabilidad'];   
			        $final[] = $arr;
			    }
			    $final = json_decode (json_encode ($final), FALSE);
			return $final;
	}