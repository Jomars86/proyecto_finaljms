<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
date_default_timezone_set('America/Guayaquil');	

	#login de usuario conectado
	function fechaActualLetras(){
		$monthDay=array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

		$weekDay=array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sabado");

		$fecha =  utf8_decode($weekDay[date("w")])." ".date("d")." de ".$monthDay[date("n")]." de ".date("Y"); 
		
		return $fecha;
	}

	function fechaHoraActualLetras(){
		$monthDay=array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

		$weekDay=array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sabado");

		$fecha =  utf8_decode($weekDay[date("w")])." ".date("d")." de ".$monthDay[date("n")]." de ".date("Y"); 
		
		$hora = strftime("a las %H:%M");

		return $fecha." ".$hora;
	}

	function fechaLetras($fecha){
		$fecha = explode("-", $fecha);
		$monthDay=array("","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
		$de = $fecha[0] < 2000 ? " de " : " del ";
		$fecha =  utf8_decode($fecha[2]." de ".$monthDay[intval($fecha[1])].$de.$fecha[0]); 
	
		return $fecha;
	}

	function fechaHoraActual(){
	    $fechaHoraActual = date("Y-m-d h:i:s");	
	    return $fechaHoraActual;
	}

	function fechaActualDMA(){
	    $fechaHoraActual = date("d/m/Y");	
	    return $fechaHoraActual;
	}
	
	function textoGradoCurso($aliasSeccion){
		switch($aliasSeccion){
			case 'EDIN':
				$gradoCurso = "NIVEL: "; 
			break;
			case 'EDBE':
				$gradoCurso = "GRADO: "; 
			break;
			case 'EDBM':
				$gradoCurso = "GRADO: "; 
			break;
			case 'EDBS':
				$gradoCurso = "CURSO: "; 
			break;
			case 'BACH':
				$gradoCurso = "CURSO: "; 
			break;
			default:
				$gradoCurso = "GRADO/CURSO: "; 
			break;
		}

		return $gradoCurso;		
	}

	function datosUsuarioInsertar($idUsuario){
		$arrayUsuario = array(
						"cci" => $idUsuario,
						"ccd" => date("Y-m-d H:i:s")
						);
		return $arrayUsuario;
	}	

	function datosUsuarioEditar($idUsuario){
		$arrayUsuario = array(
						"cwi" => $idUsuario,
						"cwd" => date("Y-m-d H:i:s")
						);
		return $arrayUsuario;
	}


	function normalizaCadena($cadena){
	    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
	ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
	    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
	bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
	    $cadena = utf8_decode($cadena);
	    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
	    $cadena = strtolower($cadena);
	    return utf8_encode($cadena);
	}

	function convertirTextoMinusculas($cadena){
		$search = array("Á", "É", "Í", "Ó", "Ú", "á", "é", "í", "ó", "ú", "Ñ", "ñ");
		$replace = array("A[a]", "E[a]", "I[a]", "O[a]", "U[a]", "a[a]", "e[a]", "i[a]", "o[a]", "u[a]", "N[a]", "n[a]");

		$texto_salida = str_replace($search, $replace, $cadena);
		$tipo_titulo = ucwords(strtolower($texto_salida));

		return str_replace($replace, $search, $tipo_titulo); // Salida: Áéíóú Áéíóú Mayúsculas Ábaco Ñ
	}

	function nombreCursoLetras($nombreCurso){
		switch ($nombreCurso) {
			case 'PRIMERO':
				$nombreCurso = "PRIMER";
				break;
			case 'TERCERO':
				$nombreCurso = "TERCER";
				break;
			
			default:
				$nombreCurso = $nombreCurso;
				break;
		}		
		return $nombreCurso;
	}

	function especialidadAlumno($aliasSeccion, $nombreParalelo){
		switch ($aliasSeccion) {
			case 'EDIN':
				$especialidad = "";
				break;
			case 'EDBE':
				$especialidad = "";
				break;
			case 'EDBS':
				$especialidad = "";
				break;
			case 'BACH':
				$nombreParalelo = explode("-", $nombreParalelo);
				if(count($nombreParalelo)>1){
					if($nombreParalelo[0] == "BACHILLERATO GENERAL UNIFICADO "){
						$especialidad = "";
					}
					else{
						$especialidad = $nombreParalelo[0]." Figura Profesional ".$nombreParalelo[1];
					}
				}
				else{
					$especialidad = "";
				}
				break;
			default:
				$especialidad = "";
				break;
		}	
		return $especialidad;	
	}

    function calcularEdad($fecha){
		$fecha_nac = new DateTime(date('Y/m/d',strtotime($fecha))); // Creo un objeto DateTime de la fecha ingresada
		$fecha_hoy =  new DateTime(date('Y/m/d',time())); // Creo un objeto DateTime de la fecha de hoy
		$edad = date_diff($fecha_hoy,$fecha_nac); // La funcion ayuda a calcular la diferencia, esto seria un objeto
		return $edad;
	}

	function textoMayuscula($texto){
		$texto = strtr(strtoupper($texto),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
		return $texto;
	}

	function textoMinuscula($texto){
		$texto = strtr(strtolower($texto),"ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ","àèìòùáéíóúçñäëïöü");
		return $texto;
	}

	function verificarPagoPesionAlumno($idAlumno, $mesPagoIndividual, $idPeriodoAcademico){
		$ControlesPensiones =& get_instance();
		$ControlesPensiones->load->model('ControlesPensiones/ControlPension');

		$pagoPension = $ControlesPensiones->ControlPension->verificarPagoPorAlumno($idAlumno, $mesPagoIndividual, $idPeriodoAcademico);

		if(is_array($pagoPension)){
			if(!$pagoPension->mesPago){
				$mensajePago = "NO PAGO ".strtoupper($mesPagoIndividual);
			}
			else{
				$mensajePago = "";
			}
		}
		else{
			$mensajePago = "";
		}
		return $mensajePago;		
	}



/* End of file fecha_helper.php */
/* Location: ./application/helpers/fecha_helper.php */

?>