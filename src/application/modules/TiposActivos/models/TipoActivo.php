<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class TipoActivo extends CI_Model{


		public function buscarTipoActivo(){
			$sql = "SELECT id as idTipoActivo, nombre as nombreTipoActivo FROM tipo_activo";
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarTipoActivo($data){
			$this->db->insert('tipo_activo', $data);
			return $this->db->insert_id();			
		}


		public function editarTipoActivo($idTipoActivo, $data){
			$this->db->where('id', $idTipoActivo);
			$this->db->update('tipo_activo', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarTipoActivo($idTipoActivo){
			$this->db->where('id', $idTipoActivo);
			$this->db->delete('tipo_activo');
			return $this->db->affected_rows();			
		}

		public function buscarTipoActivoPorID($idTipoActivo){
			$sql = "SELECT id as idTipoActivo, nombre as nombreTipoActivo FROM tipo_activo WHERE id=".$idTipoActivo;
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->row();
			}
			else{
				return false;
			}

		}

	}
?>