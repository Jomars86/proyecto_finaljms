<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TiposActivos extends MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('CategoriasMenus/CategoriaMenu');
		$this->load->model('PerfilesMenus/PerfilMenu');
		$this->load->model('Menus/Menu');
		$this->load->model('TiposActivos/TipoActivo');
		$this->load->helper('session_helper');	
		$this->load->helper('utilidades_helper');	
	}
	
	public function index($idMenu){
	  $urlCode = $idMenu;
	  $idMenu = desencriptar($idMenu);
	  if(verficarAcceso($idMenu)){
	    $dataSession = verificarPrivilegios($idMenu);
		//$idEmpresa = $this->session->userdata("idEmpresa");
	    $data['status'] = $dataSession->status;
		$data['menuNombre'] = $dataSession->nombreMenu;
		$data['codigoCategoriaMenu'] = $dataSession->codigoCategoriaMenu;
		$data['codigoMenu'] = $dataSession->codigoMenu;
		$data['urlCode'] = $urlCode;
		//Vista
		$data['tipoActivo'] = $this->TipoActivo->buscarTipoActivo();
		$data['view'] = 'TiposActivos/index';
		$data['output'] = '';
		$this->load->view('Modulos/main_administrativo',$data);

	  }
	  else{ 
	  	redirect('Login/Login');
	  }
	}
	public function lista(){
		$data['lista'] = $this->TipoActivo->buscarTipoActivo();
		$urlCode = $this->input->post("urlCode");
		$idMenu = desencriptar($urlCode);
		$dataSession = verificarPrivilegios($idMenu);
		$data['status'] = $dataSession->status;
		$this->load->view('TiposActivos/lista',$data);
	}

	public function gestionRegistro(){
		$idTipoActivo = $this->input->post("idTipoActivo");
		$nombreTipoActivo = $this->input->post("nombreTipoActivo");

		$data = array(
					"nombre" => $nombreTipoActivo
				);

		if($idTipoActivo > 0){
			echo json_encode('e|'.$this->TipoActivo->editarTipoActivo($idTipoActivo, $data));
		}
		else{
			echo json_encode('i|'.$this->TipoActivo->insertarTipoActivo($data));
		}
	}

	public function buscarTipoActivoPorID(){
		$idTipoActivo = $this->input->post("idTipoActivo");
		$data = $this->TipoActivo->buscarTipoActivoPorID($idTipoActivo);
		print_r(json_encode($data));		
	}

	public function eliminarRegistro(){
		$idTipoActivo = $this->input->post("idTipoActivo");
			
			$resultado = $this->TipoActivo->eliminarTipoActivo($idTipoActivo);
			if($resultado){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
	}
}