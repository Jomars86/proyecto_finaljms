            <!-- income order visit user End -->
            <div class="dashtwo-order-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="dashtwo-order-list shadow-reset">
                                <div class="row">
                                    <div class="col-lg-3">
                                    	<button type="button" class="btn btn-success bnt-sm" onclick="cargarTabla1();"><i class="fa fa-file"></i> General</button>
                                    </div>
                                    <div class="col-lg-3">
                                    	<button type="button" class="btn btn-success bnt-sm" onclick="cargarTablaVulnerabilidad();"><i class="fa fa-file"></i> Vulnerabilidad</button>
                                    </div>
                                    <div class="col-lg-3">
                                    	<button type="button" class="btn btn-success bnt-sm" onclick="cargarTablaAmenaza();"><i class="fa fa-file"></i> Amenaza</button>
                                    </div>
                                    <div class="col-lg-3">
                                    	<button type="button" class="btn btn-success bnt-sm" onclick="cargarTablaResumen();"><i class="fa fa-file"></i> Resumen</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12" id="listadoDatos">
                        	
                        </div>
                    </div>
                </div>
            </div>


<script type="text/javascript">
  $("#tituloPagina").text("<?php echo $menuNombre; ?>");

  function cargarTabla1(){
    cargarGif();
      $("#listadoDatos").load("<?php echo site_url('Estadisticas/tabla1'); ?>",{}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Datos");
          cerrarGif();
        }
      });  	
  }

  function cargarTablaVulnerabilidad(){
    cargarGif();
      $("#listadoDatos").load("<?php echo site_url('Estadisticas/vulnerabilidad'); ?>",{}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Datos");
          cerrarGif();
        }
      }); 
  }

  function cargarTablaAmenaza(){
    cargarGif();
      $("#listadoDatos").load("<?php echo site_url('Estadisticas/amenaza'); ?>",{}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Datos");
          cerrarGif();
        }
      }); 
  }

  function cargarTablaResumen(){
    cargarGif();
      $("#listadoDatos").load("<?php echo site_url('Estadisticas/resumen'); ?>",{}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Datos");
          cerrarGif();
        }
      }); 
  }

</script>