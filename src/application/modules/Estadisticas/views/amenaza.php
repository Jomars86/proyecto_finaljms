
<?php if($lista){ ?>
	<table id="table">
		<thead>
		   <tr>
		   	<th>N</th>
			<th>AMENAZAS</th>
			<th>IMPACTO</th>
		  </tr>
		</thead>
		<tbody>
            <?php $i=1; $acu=0; $veces = 0; $orden = 1; $elementos=sizeof($lista);?>
            <?php foreach ($lista as $lt) { ?>
              <tr>
                  <td><?php echo $orden; $orden++; ?></td>  
                  <td><?php echo $lt->nombreAmenaza; ?></td>  
                  <td><?php echo $lt->impactoAmenaza; ?></td>  
              </tr>	
            <?php } ?>    
        </tbody>        	
	</table>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

   <script type="text/javascript">
       $('#table').bootstrapTable();
   </script>