
<?php if($amenaza){ ?>
	<center><h3>AMENAZAS</h3></center>
	<table id="tableAmenaza">
		<thead>
		   <tr>
		   	<th>N</th>
			<th>AMENAZAS</th>
			<th>IMPACTO</th>
			<th>FRECUENCIA</th>
			<th>RIESGO</th>
			<th>DESCRIPCION</th>
		  </tr>
		</thead>
		<tbody>
            <?php $i=1; $acu=0; $veces = 0; $orden = 1; $elementos=sizeof($amenaza);?>
            <?php foreach ($amenaza as $lt) { ?>
              <tr>
                  <td><?php echo $orden; $orden++; ?></td>  
                  <td><?php echo $lt->nombreAmenaza; ?></td>  
                  <td><?php echo $lt->impactoAmenaza; ?></td>  
                  <td></td>  
                  <td></td>  
                  <td></td>  
              </tr>	
            <?php } ?>    
        </tbody>        	
	</table>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>


	<center><h3>VALORES</h3></center>
	<table id="tableValores">
		<thead>
		   <tr>
		   	<th colspan="3"><center>IMPACTO</center></th>
			<th></th>
			<th colspan="5"><center>PROBABILIDAD</center></th>
		  </tr>
		   <tr>
		   	<th>RANGO</th>
			<th>VALOR</th>
			<th>VALOR</th>
			<th></th>
			<th>RANGO</th>
			<th>VALOR</th>
			<th>PROBABILIDAD</th>
			<th>DESCRIPCION</th>
			<th>FRECUENCIA</th>
		  </tr>
		</thead>
		<tbody>
              <tr>
                  <td>Insignificante</td>  
                  <td>0</td>  
                  <td>20</td>  
                  <td></td>  
                  <td>Insignificante</td>  
                  <td>F</td>  
                  <td>20</td>  
                  <td>Muy poco Frecuente</td>  
                  <td>Siglos</td>  
              </tr>	
              <tr>
                  <td>Menor</td>  
                  <td>21</td>  
                  <td>40</td>  
                  <td></td>  
                  <td>Menor</td>  
                  <td>M</td>  
                  <td>25</td>  
                  <td>Poco Frecuente</td>  
                  <td>Cada varios</td>  
              </tr>	
              <tr>
                  <td>Moderado</td>  
                  <td>41</td>  
                  <td>60</td>  
                  <td></td>  
                  <td>Moderado</td>  
                  <td>MO</td>  
                  <td>47</td>  
                  <td>Normal</td>  
                  <td>Anual</td>  
              </tr>	
              <tr>
                  <td>Mayor</td>  
                  <td>61</td>  
                  <td>80</td>  
                  <td></td>  
                  <td>Mayor</td>  
                  <td>MA</td>  
                  <td>76</td>  
                  <td>Frecuente</td>  
                  <td>Mensual</td>  
              </tr>	
              <tr>
                  <td>Catastrófico</td>  
                  <td>81</td>  
                  <td>100</td>  
                  <td></td>  
                  <td>Catastrófico</td>  
                  <td>CA</td>  
                  <td>100</td>  
                  <td>Muy Frecuente</td>  
                  <td>A diario</td>  
              </tr>	
        </tbody>        	
	</table>


	<center><h3>COMPARATIVO</h3></center>
	<table id="tableComparativo" width="30%" border="1">
		<thead>
		   <tr>
		   	<th></th>
			<th></th>
			<th>RIESGO</th>
		  </tr>
		</thead>
		<tbody>
              <tr style="background-color: green;">
                  <td>0</td>  
                  <td>40</td>  
                  <td>BAJO</td>  
              </tr>	
              <tr style="background-color: yellow;">
                  <td>50</td>  
                  <td>70</td>  
                  <td>MEDIO</td>  
              </tr>	
              <tr style="background-color: orange;">
                  <td>80</td>  
                  <td>90</td>  
                  <td>ALTO</td>  
              </tr>	
              <tr style="background-color: red;">
                  <td></td>  
                  <td>100</td>  
                  <td>MUY ALTO</td>  
              </tr>	
        </tbody>        	
	</table>


	<center><h3>FINAL</h3></center>
	<table id="tableComparativo" width="100%" border="1">
		<thead>
		   <tr>
		   	<th width="10%">I</th>
			<th width="10%">R</th>
			<th width="10%">F</th>
			<th width="5%">I</th>
			<th width="10%">F</th>
			<th width="5%">R</th>
			<th width="5%">I</th>
			<th width="5%">F</th>
			<th width="5%">R</th>
			<th width="5%">I</th>
			<th width="5%">F</th>
			<th width="5%">R</th>
		  </tr>
		</thead>
		<tbody>
              <tr style="background-color: green;">
                  <td>0</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>10</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>20</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>30</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	

              <tr style="background-color: green;">
                  <td>40</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>50</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>60</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>70</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>80</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>90</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
              <tr style="background-color: green;">
                  <td>100</td>  
                  <td>IF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MF</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MO</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>MA</td>  
                  <td>0</td>  
                  <td>0</td>  
                  <td>CA</td>  
                  <td>0</td>  
              </tr>	
        </tbody>        	
	</table>

   <script type="text/javascript">
       $('#tableAmenaza').bootstrapTable();
       $('#tableValores').bootstrapTable();
      // $('#tableComparativo').bootstrapTable();
   </script>