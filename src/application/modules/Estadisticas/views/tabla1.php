<?php if($lista){ ?>
	<table id="table">
		<thead>
		   <tr>
		   	<th>N</th>
			<th>ACTIVO</th>
			<th>COD VUL</th>
			<th>VULNERABILIDAD</th>
			<th>COD AMENAZA</th>
			<th>AMENAZA</th>
			<th>INTEGRIDAD</th>
			<th>CONFIDENCIALIDAD</th>
			<th>DISPONIBILIDAD</th>
			<th>IMPACTO</th>
		  </tr>
		</thead>
		<tbody>
            <?php $i=1; ?>
            <?php foreach ($lista as $lt) { ?>
              <tr>
                <td><?php echo $i; $i++; ?></td>	
                <td><?php echo $lt->nombreActivo; ?></td>	
                <td><?php echo $lt->codigoVulnerabilidad; ?></td>	
                <td><?php echo $lt->nombreVulnerabilidad; ?></td>	
                <td><?php echo $lt->codigoAmenaza; ?></td>	
                <td><?php echo $lt->nombreAmenaza; ?></td>	
                <td><?php echo $lt->integridadAmenaza; ?></td>	
                <td><?php echo $lt->confidencialidadAmenaza; ?></td>	
                <td><?php echo $lt->disponibilidadAmenaza; ?></td>	
                <td><?php echo $lt->impactoAmenaza; ?></td>	
              </tr>	
            <?php } ?>    
        </tbody>        	
	</table>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

   <script type="text/javascript">
       $('#table').bootstrapTable();
   </script>