

<?php if($lista){ ?>
	<table id="table">
		<thead>
		   <tr>
		   	<th>N</th>
			<th>VULNERABILIDADES</th>
			<th>IMPACTO</th>
		  </tr>
		</thead>
		<tbody>
            <?php $i=1; ?>
            <?php foreach ($lista as $lt) { ?>
              <tr>
                <td><?php echo $i; $i++; ?></td>	
                <td><?php echo $lt->nombreVulnerabilidad; ?></td>	
                <td><?php echo $lt->impactoAmenaza; ?></td>	
              </tr>	
            <?php } ?>    
        </tbody>        	
	</table>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

   <script type="text/javascript">
       $('#table').bootstrapTable();
   </script>