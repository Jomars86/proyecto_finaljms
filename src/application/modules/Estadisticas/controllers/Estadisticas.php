<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas extends MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('CategoriasMenus/CategoriaMenu');
		$this->load->model('PerfilesMenus/PerfilMenu');
		$this->load->model('Menus/Menu');
		$this->load->model('Activos/Activo');
		$this->load->model('TiposActivos/TipoActivo');
		$this->load->model('Vulnerabilidades/Vulnerabilidad');
		$this->load->model('Amenazas/Amenaza');

		$this->load->helper('riesgos_helper');	
		$this->load->helper('session_helper');	
		$this->load->helper('utilidades_helper');	
	}
	
	public function index($idMenu){
	  $urlCode = $idMenu;
	  $idMenu = desencriptar($idMenu);
	  if(verficarAcceso($idMenu)){
	    $dataSession = verificarPrivilegios($idMenu);
		//$idEmpresa = $this->session->userdata("idEmpresa");
	    $data['status'] = $dataSession->status;
		$data['menuNombre'] = $dataSession->nombreMenu;
		$data['codigoCategoriaMenu'] = $dataSession->codigoCategoriaMenu;
		$data['codigoMenu'] = $dataSession->codigoMenu;
		$data['urlCode'] = $urlCode;
		//Vista
		$data['view'] = 'Estadisticas/index';
		$data['output'] = '';
		$this->load->view('Modulos/main_administrativo',$data);

	  }
	  else{ 
	  	redirect('Login/Login');
	  }
	}

	public function tabla1(){
		$data['lista'] = $this->Activo->buscarActivoVulnerabilidadAmenaza();
		$this->load->view('Estadisticas/tabla1',$data);
	}

	public function vulnerabilidad(){
		//$data['lista'] = $this->Vulnerabilidad->buscarVulnerabilidadTabular();
		$data['lista'] = tabularVulnerabilidades();
		$this->load->view('Estadisticas/vulnerabilidad',$data);
	}

	public function amenaza(){
		//$data['lista'] = tabularAmenazas();
		$data['lista'] = tabularAmenazas();
		$this->load->view('Estadisticas/amenaza',$data);
	}

	public function resumen(){
		$data['amenaza'] = tabularAmenazas();
		$this->load->view('Estadisticas/resumen',$data);
	}

}	