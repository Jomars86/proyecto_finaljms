<div id="listadoDatos" class="box-body"></div>

  <!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formAmenaza"  method="post" class="animate-form" >
      <input name="idAmenaza" id="idAmenaza" type="hidden" value="">
      <div class="modal-body">

         <div class="row">
           <div class="col-sm-3">
            <label> CODIGO </label>
             <input type="text" name="codigoAmenaza" id="codigoAmenaza" class="form-control">
           </div>
           <div class="col-sm-9">
            <label> NOMBRE </label>
             <input type="text" name="nombreAmenaza" id="nombreAmenaza" class="form-control">
           </div>
         </div>

         <div class="row">
           <div class="col-sm-3">
            <label> INTEGRIDAD </label>
             <input type="text" name="integridadAmenaza" id="integridadAmenaza" class="form-control" onchange="calcularImpacto();">
           </div>
           <div class="col-sm-3">
            <label> CONFIDENCIALIDAD </label>
             <input type="text" name="confidencialidadAmenaza" id="confidencialidadAmenaza" class="form-control" onchange="calcularImpacto();">
           </div>
           <div class="col-sm-3">
            <label> DISPONIBILIDAD </label>
             <input type="text" name="disponibilidadAmenaza" id="disponibilidadAmenaza" class="form-control" onchange="calcularImpacto();">
           </div>
           <div class="col-sm-3">
            <label> IMPACTO </label>
             <input type="text" name="impactoAmenaza" id="impactoAmenaza" class="form-control" >
           </div>
         </div>

      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>

<script type="text/javascript">

  listarDatos();
  $("#tituloPagina").text("<?php echo $menuNombre; ?>");

  function listarDatos(){
    cargarGif();
    var urlCode = "<?php echo $urlCode; ?>";
      $("#listadoDatos").load("<?php echo site_url('Amenazas/lista'); ?>",{urlCode}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Categorías de Menú");
          cerrarGif();
        }
      });
  }

function gestionRegistro(aObject){
  switch($(aObject).data('accion')){
    case 'insertarRegistro':
        $("#formAmenaza")[0].reset();
        $("#idAmenaza").val("");
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Nuevo Registro");
    break;
    case 'editarRegistro':
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Editar Registro");
        editarRegistro($(aObject).data('id'));
    break;
    case 'eliminarRegistro':
       
        swal({
          title: 'Desea eliminar?',
          text: "Los datos se perderán!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
          if (result.value) {
            
                $.ajax({
                  type : 'post',
                  url  : "<?php echo site_url('Amenazas/eliminarRegistro'); ?>",
                  dataType: 'json',
                  data: {
                        idTipoActivo   : $(aObject).data('id'),
                      }
                })  
                .done(function(data){
                  if(data){
                    $().toastmessage('showSuccessToast', "Registro eliminado");
                  }else{
                    $().toastmessage('showErrorToast', "No se pudo eliminar la información (registros enlazados)");
                  }
                })
                .fail(function(){
                  $().toastmessage('showErrorToast', "Error: No se pudo eliminar la información");
                })   
                .always(function(){
                  //$("#modalFormulario").modal('hide');
                  cerrarGif();
                  listarDatos();
                });

          }
        })

    break;
    case 'cancelarRegistro':
      swal({
        title: "Desea cancelar?",
        text: "Los datos se perderán!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, cancelar!",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
      },
      function(){
          swal("Cancelado!", "Se ha cancelado la Inscripción.", "success");
          $(aObject).text('Inscribir')
          $(aObject).removeClass("btn-danger").addClass("btn-info");
          $(aObject).data('accion', 'insertarRegistro');
        listarDatos();
      });       
    break;
  }
}

$(document).ready(function() {

 $('#formAmenaza').validate({
  rules: {
   nombreAmenaza: {
    required: true
   },
   descripcionAmenaza: {
    required: true
   }
  },
  messages: {
   nombreAmenaza: {
    required: "Ingrese Amenazas"
   }

  },
  highlight: function(element) {
   $(element).closest('.row').removeClass('success').addClass('error');
  },
  success: function(element) {
  // element.text('OK!').addClass('valid').closest('.row').removeClass('error').addClass('success');
   element.addClass('valid').closest('.row').removeClass('error').addClass('success');
  },
  submitHandler: function(form) {
   // do other stuff for a valid form
   $.post("<?php echo site_url('Amenazas/gestionRegistro')?>", $("#formAmenaza").serialize())
     .done(function(data){
                         if(data){
                                data = data.replace('"','');
                                var row = data.split('|');
                                switch(row[0]){
                                    case 'i':
                                      //$('#idParaleloMateriaProfesor').val(row[1]);   
                                      //swal("Exitoso!", "Registro creado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Registro creado exitosamente");
                                    break;
                                    case 'e':
                                      //swal("Exitoso!", "Editado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Editado exitosamente");
                                      //$("#formTipoActivo")[0].reset();
                                    break;
                                }
                           //listarDatos();     
                           }else{
                                //swal("Información!", "No se pudo procesar la información", "info"); 
                                $().toastmessage('showErrorToast', "No se pudo procesar la información");
                           }
     })
     .fail(function(err){
        //swal("Información!", "Error: No se pudo procesar la información", "warning"); 
        $().toastmessage('showErrorToast', "Error: No se pudo procesar la información");
     })
     .always(function(){
      listarDatos();
       $("#modalFormulario").modal('hide');
       $("#formAmenaza").find('.error').removeClass("error");
       $("#formAmenaza").find('.success').removeClass("success");
       $("#formAmenaza").find('.valid').removeClass("valid");
     });
  }
 });
}); // end document.ready

function editarRegistro(aId){
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Amenazas/buscarAmenazaPorID'); ?>",
      dataType: 'json',
      data: {
        idAmenaza   : aId,
      },
    }).done( function(data) {
      $(data).each(function(i, v){
        $("#idAmenaza").val(v.idAmenaza);
        $("#codigoAmenaza").val(v.codigoAmenaza);
        $("#nombreAmenaza").val(v.nombreAmenaza);
        $("#integridadAmenaza").val(v.integridadAmenaza);
        $("#confidencialidadAmenaza").val(v.confidencialidadAmenaza);
        $("#disponibilidadAmenaza").val(v.disponibilidadAmenaza);
        $("#impactoAmenaza").val(v.impactoAmenaza);
      });              
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      //alert( 'Always' );
    });
}

function calcularImpacto(){
  var integridad = $("#integridadAmenaza").val();
  var confidencialidad = $("#confidencialidadAmenaza").val();
  var disponibilidad = $("#disponibilidadAmenaza").val();
  var impacto = (parseFloat(integridad)+parseFloat(confidencialidad)+parseFloat(disponibilidad))/3;
  $("#impactoAmenaza").val("");
  $("#impactoAmenaza").val(impacto.toFixed(2));
}
</script> 