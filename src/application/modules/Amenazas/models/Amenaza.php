<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Amenaza extends CI_Model{


		public function buscarAmenaza(){
			$sql = "SELECT id as idAmenaza, nombre as nombreAmenaza, codigo as codigoAmenaza, integridad as integridadAmenaza, confidencialidad as confidencialidadAmenaza, disponibilidad as disponibilidadAmenaza, impacto as impactoAmenaza FROM amenaza";
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarAmenaza($data){
			$this->db->insert('amenaza', $data);
			return $this->db->insert_id();			
		}


		public function editarAmenaza($idAmenaza, $data){
			$this->db->where('id', $idAmenaza);
			$this->db->update('amenaza', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarTipoActivo($idAmenaza){
			$this->db->where('id', $idAmenaza);
			$this->db->delete('amenaza');
			return $this->db->affected_rows();			
		}

		public function buscarTipoActivoPorID($idAmenaza){
			$sql = "SELECT id as idAmenaza, nombre as nombreAmenaza, codigo as codigoAmenaza, integridad as integridadAmenaza, confidencialidad as confidencialidadAmenaza, disponibilidad as disponibilidadAmenaza, impacto as impactoAmenaza FROM amenaza WHERE id=".$idAmenaza;
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->row();
			}
			else{
				return false;
			}

		}

		public function buscarAmenazaTabular(){
			$sql = "SELECT activo.id AS idActivo, tipo_activo.nombre AS tipoActivo, activo.nombre AS nombreActivo, activo.descripcion AS descripcionActivo, vulnerabilidad.codigo AS codigoVulnerabilidad, vulnerabilidad.nombre AS nombreVulnerabilidad, amenaza.codigo AS codigoAmenaza, amenaza.nombre AS nombreAmenaza, amenaza.integridad AS integridadAmenaza, amenaza.confidencialidad AS confidencialidadAmenaza, amenaza.disponibilidad AS disponibilidadAmenaza, amenaza.impacto AS impactoAmenaza FROM activo INNER JOIN activo_vulnerabilidad ON activo_vulnerabilidad.activo_id = activo.id INNER JOIN vulnerabilidad ON activo_vulnerabilidad.vulnerabilidad_id = vulnerabilidad.id INNER JOIN vulnerabilidad_amenaza ON vulnerabilidad_amenaza.vulnerabilidad_id = vulnerabilidad.id INNER JOIN amenaza ON vulnerabilidad_amenaza.amenaza_id = amenaza.id INNER JOIN tipo_activo ON activo.tipo_activo_id = tipo_activo.id ORDER BY nombreAmenaza ASC";
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->result();
			}
			else{
				return false;
			}

		}


	}
?>