<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios/Usuario');

		$this->load->helper('session_helper');	
		$this->load->helper('utilidades_helper');	
	}
	
	public function index(){
		$this->load->view('Login/index');
		//echo $this->encryption->encrypt(123);
	}

	public function validarAutenticacion(){
		$emailUsuario = $this->security->xss_clean(trim($this->input->post('emailUsuario')));
		$clave = $this->security->xss_clean(trim($this->input->post('clave')));

		if(strpos($emailUsuario,'@')){
				$usr = $this->Usuario->validarUsuarioEmail($emailUsuario);
			}
			else{
				$usr = $this->Usuario->validarUsuario($emailUsuario);
			}

			if(is_object($usr->row())){
				$usuario = $usr->row();
				$claveUsuario = $this->encryption->decrypt($usuario->claveUsuario);
				echo ($claveUsuario);
				if($usuario->esSuperAdminUsuario == 'S'){
					$skin = "red";
				}
				else{
					$skin = "blue";
				}

				if( $claveUsuario == $clave ){
					$data = array(
								'idUsuario'=>$usuario->idUsuario,
								'emailUsuario'=>$usuario->emailUsuario,
								'esSuperadmin'=>$usuario->esSuperAdminUsuario,
								'nombreUsuario'=>$usuario->nombreUsuario,
								'apellidoUsuario'=>$usuario->apellidoUsuario,
								'usuario'=>$usuario->usuarioUsuario,
								'skin'=>$skin,
								'fotoUsuario'=>$usuario->fotoUsuario,
								'login'=>true,
					);
					$this->session->set_userdata( $data );
					
					redirect('Modulos/escritorio');

				}else{
					$data['output']['mensaje']=array('tipo'=>'danger', 'valor'=>'Clave incorrecta');
					$this->load->view('Login', $data);
				}
				
			}else{
				$data['output']['mensaje']=array('tipo'=>'success', 'valor'=>'Usuario incorrecto/inactivo');
				$this->load->view('Login', $data);
			}		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}
		
}