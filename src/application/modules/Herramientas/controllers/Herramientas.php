<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Herramientas extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Aseguradoras/Aseguradora');
		$this->load->model('Paises/Pais');
		$this->load->model('Provincias/Provincia');
		$this->load->model('Ciudades/Ciudad');
		$this->load->model('Parroquias/Parroquia');
		$this->load->model('Escolaridades/Escolaridad');
		$this->load->model('Profesiones/Profesion');
		$this->load->model('Ocupaciones/Ocupacion');
		$this->load->model('RutasEvacuaciones/RutaEvacuacion');
	}
	
	public function gestionRegistro(){
		$idRegistro = $this->input->post("idRegistro");
		$referencia = ucwords($this->input->post("referencia"));
		$caracter = strpos($referencia, "_");
		if($caracter > 0){
			$referencia = explode("_", $referencia);
			$referencia = $referencia[0].ucfirst($referencia[1]);
		}
		$preObjeto = ucwords($this->input->post("preObjeto"));
		$idPreObjeto = ucwords($this->input->post("idPreObjeto"));

		if($idRegistro > 0){
			$data = array('nombre' => strtoupper($this->input->post("nombreRegistro")));
			$funcion = "editar".$referencia;
			echo json_encode('e|'.$this->$referencia->$funcion($idRegistro, $data));
		}
		else{
			$data = array('nombre' => strtoupper($this->input->post("nombreRegistro")));	

			if($idPreObjeto > 0 && $preObjeto !=""){
				$dataPreObjeto = array($preObjeto.'_id' => $idPreObjeto);
				$data = array_merge($data, $dataPreObjeto);
			}
			$funcion = "insertar".$referencia;
			echo json_encode('i|'.$this->$referencia->$funcion($data));
		}
	}

	public function eliminarRegistro(){
		$idRegistro = $this->input->post("idRegistro");
		$referencia = ucwords($this->input->post("referencia"));
		$caracter = strpos($referencia, "_");
		if($caracter > 0){
			$referencia = explode("_", $referencia);
			$referencia = $referencia[0].ucfirst($referencia[1]);
		}		
		$funcion = "eliminar".$referencia;
		// Primero buscar si no hay registros asociados
		//$registrosMateria = $this->Materia->buscarMateriasRegistro($idRegistro);
		$registrosMateria = false;
		// Si existen registros eliminados no se puede eliminar
		if($registrosMateria){
			echo json_encode(false);
		}// si no hay nada asociado se puede eliminar
		else{
			$resultado = $this->$referencia->$funcion($idRegistro);
			if($resultado){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
		}
	}

	public function recargarSelect(){
		$referencia = ucwords($this->input->post("referencia"));
		$caracter = strpos($referencia, "_");
		if($caracter > 0){
			$referencia = explode("_", $referencia);
			$referencia = $referencia[0].ucfirst($referencia[1]);
		}		
		$idPreObjeto = $this->input->post("idPreObjeto");
		$funcion = "buscar".$referencia;
		if($idPreObjeto > 0){
			//echo "1";
			$data = $this->$referencia->$funcion($idPreObjeto);
		}
		else{
			//echo "2";
			$data = $this->$referencia->$funcion();
		}
		print_r(json_encode($data));

	}

}