<style type="text/css">
 @media screen and (min-width: 500px) {
  #modalFormulario .modal-dialog  {width:50%;}
 }
</style>

  <!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formRegistro"  method="post" class="animate-form" >
      <input name="idRegistro" id="idRegistro" type="hidden" value="">
      <input name="referencia" id="referencia" type="hidden" value="">
      <div class="modal-body">
        
        <div class="row">
          <div class="col-sm-12">
            <label for="nombreRegistro">Nombre: <span class="kv-reqd">*</span></label>
              <input type="text" name="nombreRegistro" id="nombreRegistro" class="form-control requiered">
          </div>
        </div>  


      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="button" id="botonCrear" class="btn btn-success" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="nuevo"><span class="glyphicon glyphicon-file"></span> Crear</button>
        <button type="button" id="botonEditar" class="btn btn-info" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="editar"><span class="glyphicon glyphicon-edit"></span> Editar</button>
        <button type="button" id="botonEliminar" class="btn btn-danger" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="eliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" style="width: 100px;"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>

<script type="text/javascript">

function gestionarRegistroModal(aObject){
  var accion = $(aObject).data("accion");
  switch(accion){
    case 'eliminar':
        swal({
          title: 'Desea eliminar?',
          text: "Los datos se perderán!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then(function () {
         
          $.ajax({
            type : 'post',
            url  : "<?php echo site_url('Herramientas/eliminarRegistro'); ?>",
            dataType: 'json',
            data: {
                  idRegistro   : $("#idRegistro").val(),
                  referencia   : $("#referencia").val(),
                }
          })  
          .done(function(data){
              if(data){
                $().toastmessage('showSuccessToast', "Registro eliminado");
              }else{
                $().toastmessage('showErrorToast', "No se pudo eliminar la información");
              }
            })
            .fail(function(){
              $().toastmessage('showErrorToast', "Error: No se pudo eliminar la información");
            })   
            .always(function(){
              $("#formRegistro")[0].reset();
              $("#modalFormulario").modal('hide');
              recargarSelect("id"+ucwords(($("#referencia").val())), $("#referencia").val(), "");
            });
          })

    break;
    default:
      var nombreRegistro = $("#nombreRegistro").val();
      if(nombreRegistro){
       $.post("<?php echo site_url('Herramientas/gestionRegistro')?>", $("#formRegistro").serialize())
        .done(function(data){
          if(data){
            data = data.replace('"','');
            var row = data.split('|');
            switch(row[0]){
              case 'i':
                $().toastmessage('showSuccessToast', "Registro creado exitosamente");
              break;
              case 'e':
                $().toastmessage('showSuccessToast', "Editado exitosamente");
              break;
            }
          }else{
            $().toastmessage('showErrorToast', "No se pudo procesar la información");
          }
        })
         .fail(function(err){
            //swal("Información!", "Error: No se pudo procesar la información", "warning"); 
            $().toastmessage('showErrorToast', "Error: No se pudo procesar la información");
         })
         .always(function(){
            $("#formRegistro")[0].reset();
            $("#modalFormulario").modal('hide');
            recargarSelect("id"+ucwords(($("#referencia").val())), $("#referencia").val(), nombreRegistro);
         });

      }
      else{
        $().toastmessage('showErrorToast', "El nombre no puede estar vacío.");
      }
    break;
  }  
}


function recargarSelect(aObjectID, aReference, aText){
  aText = aText.toUpperCase();
  //alert(aText);
  cargarGif();
  $.ajax({
    url      : "<?php echo site_url('Herramientas/recargarSelect');?>",
    type     : 'post',
    dataType : 'json',
    data     :{
                referencia:aReference
             },
    success: function(data){
      //alert(aObjectID);
      aObjectID = trim(aObjectID);
      $("#"+aObjectID).find('option').remove();
      $(data).each(function(i, v){
        //alert(v.nombre);
        $("#"+aObjectID).append('<option value="'+ v.id +'">' + v.nombre+ '</option');
      })
      $("#"+aObjectID).append('<option value="new">___NUEVO___</option');
      if(aText){
        $("#"+aObjectID).find('option:contains("' + aText + '")').attr('selected', true);
      }

    },
    complete: function(){
      cerrarGif();
    },
    error: function(){
      $().toastmessage('showErrorToast', "No se puede recargar el Select.");
    }
  });
}

function ucwords(str) {
    strVal = '';
    str = str.split(' ');
    for (var chr = 0; chr < str.length; chr++) {
        strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
    }
    return strVal
}

function trim(cadena){
  // USO: Devuelve un string como el
  // parámetro cadena pero quitando los
  // espacios en blanco de los bordes.

  var retorno=cadena.replace(/^\s+/g,'');
  retorno=retorno.replace(/\s+$/g,'');
  return retorno;
}

</script>