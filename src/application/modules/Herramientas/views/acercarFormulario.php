<style type="text/css">
 @media screen and (min-width: 500px) {
  #modalFormulario .modal-dialog  {width:50%;}
 }
</style>

  <!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formRegistro"  method="post" class="animate-form" >
      <input name="idRegistro" id="idRegistro" type="hidden" value="">
      <input name="referencia" id="referencia" type="hidden" value="">
      <input name="idObjeto" id="idObjeto" type="hidden" value="">
      <input name="idPreObjeto" id="idPreObjeto" type="hidden" value="">
      <input name="preObjeto" id="preObjeto" type="hidden" value="">
      <div class="modal-body">
        
        <div class="row">
          <div class="col-sm-12">
            <label for="nombreRegistro">Nombre: <span class="kv-reqd">*</span></label>
              <input type="text" name="nombreRegistro" id="nombreRegistro" class="form-control requiered">
          </div>
        </div>  


      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="button" id="botonCrear" class="btn btn-success" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="nuevo"><span class="glyphicon glyphicon-file"></span> Crear</button>
        <button type="button" id="botonEditar" class="btn btn-info" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="editar"><span class="glyphicon glyphicon-edit"></span> Editar</button>
        <button type="button" id="botonEliminar" class="btn btn-danger" style="width: 100px;" onclick="gestionarRegistroModal(this);" data-accion="eliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal" style="width: 100px;"><span class="glyphicon glyphicon-remove"></span> Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>

<script type="text/javascript">

function accionesAcercar(aObject){

  var entidad = $(aObject).data("objeto");
  var pos = entidad.indexOf("_");
  if(pos > 0){
    var res = entidad.split("_");
    var tituloEntidad = primeraMayuscula(res[0])+" "+primeraMayuscula(res[1]);
  }
  else{
    var tituloEntidad = primeraMayuscula(entidad);
  }

  var nombreIdObjeto = "#"+$(aObject).data("idobjeto");
  $("#idObjeto").val($(aObject).data("idobjeto"));
  var idObjeto = $(nombreIdObjeto).val();

  var preobjeto = $(aObject).data("preobjeto");
  //var nombrePreObjeto = "#id"+primeraMayuscula(preobjeto);
  var nombrePreObjeto = $(aObject).data("idpreobjeto");
  
  if(nombrePreObjeto != ""){
    var idPreObjeto = $("#"+nombrePreObjeto).val();
    $("#idPreObjeto").val(idPreObjeto);
    $("#preObjeto").val(preobjeto);
  }
  else{
    $("#idPreObjeto").val("");
  }
  /*

  var objeto = "id"+().substring(0, 1).toUpperCase(); //pais
  alert(objeto);
  var idObjeto = $(objeto).val();
  var referencia = $(aObject).data("objeto");
  */
  switch(idObjeto){
    case "new":
      $("#modalFormulario").modal('show');
      $("#tituloModal").text("Crear "+tituloEntidad);
      $("#idRegistro").val("");
      $("#nombreRegistro").val("");
      $("#referencia").val(entidad);
      $("#botonCrear").show();
      $("#botonEditar").hide();
      $("#botonEliminar").hide();
    break;
    default:
      if(idObjeto > 0){
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Editar/Eliminar "+tituloEntidad);
        $("#idRegistro").val(idObjeto);
        $("#nombreRegistro").val($(nombreIdObjeto+" option:selected").text());
        $("#referencia").val(entidad);
        $("#botonCrear").hide();
        $("#botonEditar").show();
        $("#botonEliminar").show();
      }
      else{
        $().toastmessage('showErrorToast', "La selección es incorrecta");
      }
    break;
  }    
}

function primeraMayuscula(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function gestionarRegistroModal(aObject){
  var accion = $(aObject).data("accion");
  var preObjeto = $("#idPreObjeto").val();
  var nombreIdObjeto = "#"+$("#idObjeto").val();
  //alert(preObjeto);
  switch(accion){
    case 'eliminar':

        Swal({
          title: 'Desea eliminar?',
          text: 'Los datos se perderán!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Si',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
              $.ajax({
                type : 'post',
                url  : "<?php echo site_url('Herramientas/eliminarRegistro'); ?>",
                dataType: 'json',
                data: {
                      idRegistro   : $("#idRegistro").val(),
                      referencia   : $("#referencia").val(),
                    }
              })  
              .done(function(data){
                  if(data){
                    $().toastmessage('showSuccessToast', "Registro eliminado");
                  }else{
                    $().toastmessage('showErrorToast', "No se pudo eliminar la información");
                  }
                })
                .fail(function(){
                  $().toastmessage('showErrorToast', "Error: No se pudo eliminar la información");
                })   
                .always(function(){
                  $("#formRegistro")[0].reset();
                  $("#modalFormulario").modal('hide');
                  recargarSelect(nombreIdObjeto, $("#referencia").val(), "", preObjeto);
                });
          // For more information about handling dismissals please visit
          // https://sweetalert2.github.io/#handling-dismissals
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal(
              'Cancelado',
              'El registro se conservará :)',
              'error'
            )
          }
        })

    break;
    default:
      var nombreRegistro = $("#nombreRegistro").val();
      if(nombreRegistro){
       $.post("<?php echo site_url('Herramientas/gestionRegistro')?>", $("#formRegistro").serialize())
        .done(function(data){
          if(data){
            data = data.replace('"','');
            var row = data.split('|');
            switch(row[0]){
              case 'i':
                $().toastmessage('showSuccessToast', "Registro creado exitosamente");
              break;
              case 'e':
                $().toastmessage('showSuccessToast', "Editado exitosamente");
              break;
            }
          }else{
            $().toastmessage('showErrorToast', "No se pudo procesar la información");
          }
        })
         .fail(function(err){
            //swal("Información!", "Error: No se pudo procesar la información", "warning"); 
            $().toastmessage('showErrorToast', "Error: No se pudo procesar la información");
         })
         .always(function(){
            $("#formRegistro")[0].reset();
            $("#modalFormulario").modal('hide');
            recargarSelect(nombreIdObjeto, $("#referencia").val(), nombreRegistro, preObjeto);
         });

      }
      else{
        $().toastmessage('showErrorToast', "El nombre no puede estar vacío.");
      }
    break;
  }  
}


function recargarSelect(aObjectID, aReference, aText, idPreObjeto){
  aText = aText.toUpperCase();
  //alert(aText);
  cargarGif();
  $.ajax({
    url      : "<?php echo site_url('Herramientas/recargarSelect');?>",
    type     : 'post',
    dataType : 'json',
    data     :{
                referencia : aReference,
                idPreObjeto : idPreObjeto
             },
    success: function(data){
      //alert(aObjectID);
     // aObjectID = trim(aObjectID);
      $(aObjectID).find('option').remove();
      $(aObjectID).append('<option value="">SELECCIONE...</option');
      $(aObjectID).append('<option value="new">___NUEVO___</option');
      $(data).each(function(i, v){
        //alert(v.nombre);
        $(aObjectID).append('<option value="'+ v.id +'">' + v.nombre+ '</option');
      })
      if(aText){
        $(aObjectID).find('option:contains("' + aText + '")').attr('selected', true);
      }

    },
    complete: function(){
      cerrarGif();
    },
    error: function(){
      $().toastmessage('showErrorToast', "No se puede recargar el Select.");
    }
  });
}

function ucwords(str) {
    strVal = '';
    str = str.split(' ');
    for (var chr = 0; chr < str.length; chr++) {
        strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
    }
    return strVal
}

function trim(cadena){
  // USO: Devuelve un string como el
  // parámetro cadena pero quitando los
  // espacios en blanco de los bordes.

  var retorno=cadena.replace(/^\s+/g,'');
  retorno=retorno.replace(/\s+$/g,'');
  return retorno;
}

</script>