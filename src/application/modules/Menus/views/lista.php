<div class="sparkline8-graph">
    <div class="datatable-dashv1-list custom-datatable-overright">
        <div id="toolbar" align="left">
            <button type="button" id="btngestionRegistro" class="btn btn-success bnt-sm" onclick="gestionRegistro(this);" data-titulo="<b><i class='fa fa-file'></i> Nuevo Registro</b>" data-accion="insertarRegistro" <?php echo $status; ?>><i class="fa fa-file"></i> Nuevo Registro</button>
        </div>


<?php if($lista){ ?>
    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
        <thead>
            <tr>
                <th data-field="state" data-checkbox="true"></th>
                <th data-field="id">N</th>
                <th data-field="nombre" data-editable="true">Categoría</th>
                <th data-field="codigo" data-editable="true">Menú</th>
                <th data-field="icono" data-editable="true">Icono</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            <?php foreach ($lista as $lt) { ?>
              <tr>
                <td></td>
                <td><?php echo $i; $i++; ?></td>
                <td><?php echo $lt->nombreCategoria; ?></td>
                <td><?php echo $lt->nombreMenu; ?></td>
                <td><?php echo $lt->iconoMenu; ?></td>
                <td>
                 <button type="button" class="btn btn-info bnt-sm" onclick="gestionRegistro(this);" data-titulo="<b><i class='fa fa-file'></i> Editar Registro</b>" data-accion="editarRegistro" data-id="<?php echo $lt->idMenu;?>" <?php echo $status; ?>><i class="fa fa-pencil"></i></button> 
                 <button type="button" class="btn btn-danger btn-sm" onclick="gestionRegistro(this);" data-toggle="tooltip" data-placement="left" title="Eliminar Registro" data-accion="eliminarRegistro" data-id="<?php echo $lt->idMenu; ?>" <?php echo $status; ?>><i class="fa fa-trash-o"></i>
                </button>
                </td>
              </tr>
            <?php } ?> 
        </tbody>
    </table>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

    </div>
</div>

   <script type="text/javascript">
       $('#table').bootstrapTable();
   </script>


