<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Vulnerabilidades extends MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('CategoriasMenus/CategoriaMenu');
		$this->load->model('PerfilesMenus/PerfilMenu');
		$this->load->model('Menus/Menu');
		$this->load->model('Vulnerabilidades/Vulnerabilidad');
		$this->load->helper('session_helper');	
		$this->load->helper('utilidades_helper');	
	}
	
	public function index($idMenu){
	  $urlCode = $idMenu;
	  $idMenu = desencriptar($idMenu);
	  if(verficarAcceso($idMenu)){
	    $dataSession = verificarPrivilegios($idMenu);
		//$idEmpresa = $this->session->userdata("idEmpresa");
	    $data['status'] = $dataSession->status;
		$data['menuNombre'] = $dataSession->nombreMenu;
		$data['codigoCategoriaMenu'] = $dataSession->codigoCategoriaMenu;
		$data['codigoMenu'] = $dataSession->codigoMenu;
		$data['urlCode'] = $urlCode;
		//Vista
		$data['Vulnerabilidad'] = $this->Vulnerabilidad->buscarVulnerabilidad();
		$data['view'] = 'Vulnerabilidades/index';
		$data['output'] = '';
		$this->load->view('Modulos/main_administrativo',$data);

	  }
	  else{ 
	  	redirect('Login/Login');
	  }
	}
	public function lista(){
		$data['lista'] = $this->Vulnerabilidad->buscarVulnerabilidad();
		$urlCode = $this->input->post("urlCode");
		$idMenu = desencriptar($urlCode);
		$dataSession = verificarPrivilegios($idMenu);
		$data['status'] = $dataSession->status;
		$this->load->view('Vulnerabilidades/lista',$data);
	}

	public function gestionRegistro(){
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$codigoVulnerabilidad = $this->input->post("codigoVulnerabilidad");
		$nombreVulnerabilidad = $this->input->post("nombreVulnerabilidad");

		$data = array(
					"codigo" => $codigoVulnerabilidad,
					"nombre" => $nombreVulnerabilidad
				);

		if($idVulnerabilidad > 0){
			echo json_encode('e|'.$this->Vulnerabilidad->editarVulnerabilidad($idVulnerabilidad, $data));
		}
		else{
			echo json_encode('i|'.$this->Vulnerabilidad->insertarVulnerabilidad($data));
		}
	}

	public function buscarVulnerabilidadPorID(){
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$data = $this->Vulnerabilidad->buscarVulnerabilidadPorID($idVulnerabilidad);
		print_r(json_encode($data));		
	}

	public function eliminarRegistro(){
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
			
			$resultado = $this->Vulnerabilidad->eliminarVulnerabilidad($idVulnerabilidad);
			if($resultado){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
	}

	public function buscarAmenazasDisponiblesParaVulnerabilidades(){
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$data = $this->Vulnerabilidad->buscarAmenazasDisponiblesParaVulnerabilidades($idVulnerabilidad);
		print_r(json_encode($data));
	}

	public function listaVulnerabilidadAmenaza(){
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$data['listaAmenaza'] = $this->Vulnerabilidad->buscarAmenazasDeVulnerabilidades($idVulnerabilidad);
		$this->load->view('Vulnerabilidades/listaVulnerabilidadAmenaza',$data);		
	}

	public function agregarRegistroAmenaza(){
		$idAmenaza = $this->input->post("idAmenaza");
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$data =  array("amenaza_id" => $idAmenaza,
					   "vulnerabilidad_id" => $idVulnerabilidad
					  );
		echo json_encode($this->Vulnerabilidad->insertarVulnerabilidadAmenaza($data));

	}

	public function eliminarRegistroAmenaza(){
		$idAmenaza = $this->input->post("idAmenaza");
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		echo json_encode($this->Vulnerabilidad->eliminarVulnerabilidadAmenaza($idAmenaza, $idVulnerabilidad));		
	}

}