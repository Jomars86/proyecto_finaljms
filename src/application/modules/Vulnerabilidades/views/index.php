<div id="listadoDatos" class="box-body"></div>

  <!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formVulnerabilidad"  method="post" class="animate-form" >
      <input name="idVulnerabilidad" id="idVulnerabilidad" type="hidden" value="">
      <div class="modal-body">

         <div class="row">
           <div class="col-sm-3">
            <label> CODIGO </label>
             <input type="text" name="codigoVulnerabilidad" id="codigoVulnerabilidad" class="form-control">
           </div>
           <div class="col-sm-9">
            <label> NOMBRE </label>
             <input type="text" name="nombreVulnerabilidad" id="nombreVulnerabilidad" class="form-control">
           </div>
         </div>

      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>


  <!-- Modal -->
<div id="modalAmenaza" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formVulnerabilidadAmenaza"  method="post" class="animate-form" >
      <div class="modal-body">
      <input name="idVulnerabilidadAmenaza" id="idVulnerabilidadAmenaza" type="hidden" value="">
    

         <div class="row">
            <div class="col-sm-12">
                <label for="idAmenaza">AMENAZA: <span class="kv-reqd">*</span></label>
                  <div class="input-group"> 
                    <select class="form-control" name="idAmenaza" id="idAmenaza">
                    </select>
                    <span class="input-group-btn">
                      <button id="idBotonVulnerabilidad" type="button" class="btn btn-default" onclick="agregarRegistro(this);" data-idactivo="">Agregar</button>
                    </span>  
                  </div>
              </div>
            </div>

          <div class="row">
            <div id="listaActivoVulnerabilidad" class="box-body"></div>
          </div>  

      
      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>


<script type="text/javascript">

  listarDatos();
  $("#tituloPagina").text("<?php echo $menuNombre; ?>");

  function listarDatos(){
    cargarGif();
    var urlCode = "<?php echo $urlCode; ?>";
      $("#listadoDatos").load("<?php echo site_url('Vulnerabilidades/lista'); ?>",{urlCode}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Categorías de Menú");
          cerrarGif();
        }
      });
  }

function gestionRegistro(aObject){
  switch($(aObject).data('accion')){
    case 'insertarRegistro':
        $("#formVulnerabilidad")[0].reset();
        $("#idVulnerabilidad").val("");
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Nuevo Registro");
    break;
    case 'editarRegistro':
        $("#modalFormulario").modal('show');
        $("#idVulnerabilidad").val("");
        $("#tituloModal").text("Editar Registro");
        editarRegistro($(aObject).data('id'));
    break;
    case 'eliminarRegistro':
       
        swal({
          title: 'Desea eliminar?',
          text: "Los datos se perderán!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
          if (result.value) {
            
                $.ajax({
                  type : 'post',
                  url  : "<?php echo site_url('Vulnerabilidades/eliminarRegistro'); ?>",
                  dataType: 'json',
                  data: {
                        idVulnerabilidad   : $(aObject).data('id'),
                      }
                })  
                .done(function(data){
                  if(data){
                    $().toastmessage('showSuccessToast', "Registro eliminado");
                  }else{
                    $().toastmessage('showErrorToast', "No se pudo eliminar la información (registros enlazados)");
                  }
                })
                .fail(function(){
                  $().toastmessage('showErrorToast', "Error: No se pudo eliminar la información");
                })   
                .always(function(){
                  //$("#modalFormulario").modal('hide');
                  cerrarGif();
                  listarDatos();
                });

          }
        })

    break;
    case 'cancelarRegistro':
      swal({
        title: "Desea cancelar?",
        text: "Los datos se perderán!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, cancelar!",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
      },
      function(){
          swal("Cancelado!", "Se ha cancelado la Inscripción.", "success");
          $(aObject).text('Inscribir')
          $(aObject).removeClass("btn-danger").addClass("btn-info");
          $(aObject).data('accion', 'insertarRegistro');
        listarDatos();
      });       
    break;
  }
}

$(document).ready(function() {

 $('#formVulnerabilidad').validate({
  rules: {
   nombreVulnerabilidad: {
    required: true
   },
   descripcionVulnerabilidad: {
    required: true
   }
  },
  messages: {
   nombreVulnerabilidad: {
    required: "Ingrese Vulnerabilidad"
   }

  },
  highlight: function(element) {
   $(element).closest('.row').removeClass('success').addClass('error');
  },
  success: function(element) {
  // element.text('OK!').addClass('valid').closest('.row').removeClass('error').addClass('success');
   element.addClass('valid').closest('.row').removeClass('error').addClass('success');
  },
  submitHandler: function(form) {
   // do other stuff for a valid form
   $.post("<?php echo site_url('Vulnerabilidades/gestionRegistro')?>", $("#formVulnerabilidad").serialize())
     .done(function(data){
                         if(data){
                                data = data.replace('"','');
                                var row = data.split('|');
                                switch(row[0]){
                                    case 'i':
                                      //$('#idParaleloMateriaProfesor').val(row[1]);   
                                      //swal("Exitoso!", "Registro creado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Registro creado exitosamente");
                                    break;
                                    case 'e':
                                      //swal("Exitoso!", "Editado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Editado exitosamente");
                                      //$("#formTipoActivo")[0].reset();
                                    break;
                                }
                           //listarDatos();     
                           }else{
                                //swal("Información!", "No se pudo procesar la información", "info"); 
                                $().toastmessage('showErrorToast', "No se pudo procesar la información");
                           }
     })
     .fail(function(err){
        //swal("Información!", "Error: No se pudo procesar la información", "warning"); 
        $().toastmessage('showErrorToast', "Error: No se pudo procesar la información");
     })
     .always(function(){
      listarDatos();
       $("#modalFormulario").modal('hide');
       $("#formVulnerabilidad").find('.error').removeClass("error");
       $("#formVulnerabilidad").find('.success').removeClass("success");
       $("#formVulnerabilidad").find('.valid').removeClass("valid");
     });
  }
 });
}); // end document.ready

function editarRegistro(aId){
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Vulnerabilidades/buscarVulnerabilidadPorID'); ?>",
      dataType: 'json',
      data: {
        idVulnerabilidad   : aId,
      },
    }).done( function(data) {
      $(data).each(function(i, v){
        $("#idVulnerabilidad").val(v.idVulnerabilidad);
        $("#codigoVulnerabilidad").val(v.codigoVulnerabilidad);
        $("#nombreVulnerabilidad").val(v.nombreVulnerabilidad);
      });              
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      //alert( 'Always' );
    });
}


function vulnerabilidadAmenaza(aObject){
  $("#modalAmenaza").modal('show');
  var idVulnerabilidad = $(aObject).data('id');
  $("#idVulnerabilidadAmenaza").val(idVulnerabilidad);
  buscarAmenazasDisponibles(idVulnerabilidad);
  buscarAmenazasAsociadoVulnerabilidad(idVulnerabilidad);
}

function buscarAmenazasDisponibles(idVulnerabilidad){
  cargarGif();
  //var idVulnerabilidad = $("#idVulnerabilidad").val();
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Vulnerabilidades/buscarAmenazasDisponiblesParaVulnerabilidades'); ?>",
      dataType: 'json',
      data: {
        idVulnerabilidad   : idVulnerabilidad,
      },
    }).done( function(data) {
      $('#idAmenaza').find('option').remove();
      $(data).each(function(i, v){
        $('#idAmenaza').append('<option value="'+ v.idAmenaza +'">' + v.nombreAmenaza+ '</option');
      });
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      cerrarGif();
    });  
}

function buscarAmenazasAsociadoVulnerabilidad(idVulnerabilidad){
  //var idPerfil = $("#idPerfil").val();
  var urlCode = "<?php echo $urlCode; ?>";
  //cargarGif();
  $("#listaActivoVulnerabilidad").load("<?php echo site_url('Vulnerabilidades/listaVulnerabilidadAmenaza'); ?>",{idVulnerabilidad, urlCode}, function(responseText, statusText, xhr){
    if(statusText == "success"){
      cerrarGif();
    }
    if(statusText == "error"){
    swal("Información!", "No se pudo cargar la información", "info"); 
      //cerrarGif();
    }
  });
}


function agregarRegistro(aObject){
  var idAmenaza = $("#idAmenaza").val();
  if(idAmenaza){
    cargarGif();
    var idVulnerabilidad = $("#idVulnerabilidadAmenaza").val();
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Vulnerabilidades/agregarRegistroAmenaza'); ?>",
      dataType: 'json',
      data: {
        idVulnerabilidad : idVulnerabilidad,
        idAmenaza   : idAmenaza,
      },
    }).done( function(data) {
        if(data){

              $().toastmessage('showSuccessToast', "Registro creado exitosamente");

        }else{
          $().toastmessage('showErrorToast', "No se pudo procesar la información");
        }
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      buscarAmenazasDisponibles(idVulnerabilidad);
      buscarAmenazasAsociadoVulnerabilidad(idVulnerabilidad);
      cerrarGif();
    });  
  }
}



function eliminarRegistro(aObject){
  
  var idVulnerabilidad = $("#idVulnerabilidadAmenaza").val();

  swal({
    title: 'Desea eliminar?',
    text: "Los datos se perderán!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.value) {
         $.ajax({
        type : 'post',
        url  : "<?php echo site_url('Vulnerabilidades/eliminarRegistroAmenaza'); ?>",
        dataType: 'json',
        data: {
          idVulnerabilidad : idVulnerabilidad,
          idAmenaza : $(aObject).data("idamenaza")
        },
      }).done( function(data) {
        if(data){
          $().toastmessage('showSuccessToast', "Registro eliminado");
        }else{
          $().toastmessage('showErrorToast', "No se pudo eliminar la información (registros enlazados)");
        }
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      buscarAmenazasDisponibles(idVulnerabilidad);
      buscarAmenazasAsociadoVulnerabilidad(idVulnerabilidad);
      cerrarGif();
    }); 
    }
  }) 
}
</script> 