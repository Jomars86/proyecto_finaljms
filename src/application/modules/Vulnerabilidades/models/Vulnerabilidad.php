<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Vulnerabilidad extends CI_Model{


		public function buscarVulnerabilidad(){
			$sql = "SELECT id as idVulnerabilidad, codigo as codigoVulnerabilidad, nombre as nombreVulnerabilidad FROM vulnerabilidad";
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarVulnerabilidad($data){
			$this->db->insert('vulnerabilidad', $data);
			return $this->db->insert_id();			
		}


		public function editarVulnerabilidad($idVulnerabilidad, $data){
			$this->db->where('id', $idVulnerabilidad);
			$this->db->update('vulnerabilidad', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarVulnerabilidad($idVulnerabilidad){
			$this->db->where('id', $idVulnerabilidad);
			$this->db->delete('vulnerabilidad');
			return $this->db->affected_rows();			
		}

		public function buscarVulnerabilidadPorID($idVulnerabilidad){
			$sql = "SELECT id as idVulnerabilidad, codigo as codigoVulnerabilidad, nombre as nombreVulnerabilidad FROM vulnerabilidad WHERE id=".$idVulnerabilidad;
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->row();
			}
			else{
				return false;
			}

		}


		//  Vulnerabilidades Amenazas
		public function buscarAmenazasDisponiblesParaVulnerabilidades($idVulnerabilidad){
			$sql = "SELECT amenaza.id as idAmenaza, amenaza.nombre as nombreAmenaza FROM amenaza WHERE amenaza.id NOT IN (SELECT vulnerabilidad_amenaza.amenaza_id FROM vulnerabilidad_amenaza WHERE vulnerabilidad_amenaza.vulnerabilidad_id = ".$idVulnerabilidad." ) GROUP BY amenaza.id ORDER BY amenaza.nombre";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}
		}	

		public function buscarAmenazasDeVulnerabilidades($idVulnerabilidad){
			$sql = "SELECT amenaza.id AS idAmenaza, amenaza.codigo AS codigoAmenaza, amenaza.nombre AS nombreAmenaza FROM amenaza INNER JOIN vulnerabilidad_amenaza ON vulnerabilidad_amenaza.amenaza_id = amenaza.id WHERE vulnerabilidad_amenaza.vulnerabilidad_id = ".$idVulnerabilidad." GROUP BY amenaza.id ORDER BY nombreAmenaza ASC";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarVulnerabilidadAmenaza($data){
			$this->db->insert('vulnerabilidad_amenaza', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarVulnerabilidadAmenaza($idAmenaza, $idVulnerabilidad){
			$this->db->where('amenaza_id', $idAmenaza);
			$this->db->where('vulnerabilidad_id', $idVulnerabilidad);
			$this->db->delete('vulnerabilidad_amenaza');
			return $this->db->affected_rows();				
		}

		public function buscarVulnerabilidadTabular(){
			$sql = "SELECT activo.id AS idActivo, tipo_activo.nombre AS tipoActivo, activo.nombre AS nombreActivo, activo.descripcion AS descripcionActivo, vulnerabilidad.codigo AS codigoVulnerabilidad, vulnerabilidad.nombre AS nombreVulnerabilidad, amenaza.codigo AS codigoAmenaza, amenaza.nombre AS nombreAmenaza, amenaza.integridad AS integridadAmenaza, amenaza.confidencialidad AS confidencialidadAmenaza, amenaza.disponibilidad AS disponibilidadAmenaza, amenaza.impacto AS impactoAmenaza FROM activo INNER JOIN activo_vulnerabilidad ON activo_vulnerabilidad.activo_id = activo.id INNER JOIN vulnerabilidad ON activo_vulnerabilidad.vulnerabilidad_id = vulnerabilidad.id INNER JOIN vulnerabilidad_amenaza ON vulnerabilidad_amenaza.vulnerabilidad_id = vulnerabilidad.id INNER JOIN amenaza ON vulnerabilidad_amenaza.amenaza_id = amenaza.id INNER JOIN tipo_activo ON activo.tipo_activo_id = tipo_activo.id ORDER BY nombreVulnerabilidad ASC";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}			
		}

	}
?>