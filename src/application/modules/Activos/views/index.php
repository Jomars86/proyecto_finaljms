<div id="listadoDatos" class="box-body"></div>

  <!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formActivo"  method="post" class="animate-form" >
      <input name="idActivo" id="idActivo" type="hidden" value="">
      <div class="modal-body">
    

         <div class="row">
            <div class="col-sm-12">
                <label for="idTipoActivo">TIPO DE ACTIVO: <span class="kv-reqd">*</span></label>
                  <select class="form-control required" id="idTipoActivo" name="idTipoActivo">
                    <?php foreach($tipoActivo as $dp){ ?>
                    <option value="<?php echo $dp->idTipoActivo;?>"><?php echo $dp->nombreTipoActivo; ?></option>
                    <?php } ?>
                  </select>
              </div>
            </div>

         <div class="row">
           <div class="col-sm-12">
            <label> NOMBRE </label>
             <input type="text" name="nombreActivo" id="nombreActivo" class="form-control">
           </div>
         </div>

       <div class="row">
           <div class="col-sm-12">
            <label> DESCRIPCION </label>
             <textarea class="form-control" name="descripcionActivo" id="descripcionActivo"></textarea>
           </div>
         </div>
       
      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>



  <!-- Modal -->
<div id="modalVulneravilidad" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="tituloModal"></h4>
        

      </div>
     <form id="formActivoVulnerabilidad"  method="post" class="animate-form" >
      <div class="modal-body">
      <input name="idVulnerabilidadActivo" id="idVulnerabilidadActivo" type="hidden" value="">
    

         <div class="row">
            <div class="col-sm-12">
                <label for="idVulnerabilidad">VULNERABILIDAD: <span class="kv-reqd">*</span></label>
                  <div class="input-group"> 
                    <select class="form-control" name="idVulnerabilidad" id="idVulnerabilidad">
                    </select>
                    <span class="input-group-btn">
                      <button id="idBotonActivo" type="button" class="btn btn-default" onclick="agregarRegistro(this);" data-idactivo="">Agregar</button>
                    </span>  
                  </div>
              </div>
            </div>

          <div class="row">
            <div id="listaActivoVulnerabilidad" class="box-body"></div>
          </div>  

      
      </div><!-- Fin modal body -->
      <div class="modal-footer">
        <button type="submit" id="botonGuardar" class="btn btn-success">Guardar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
       </form>

    </div>

  </div>
</div>
<script type="text/javascript">

  listarDatos();
  $("#tituloPagina").text("<?php echo $menuNombre; ?>");

  function listarDatos(){
    cargarGif();
    var urlCode = "<?php echo $urlCode; ?>";
      $("#listadoDatos").load("<?php echo site_url('Activos/lista'); ?>",{urlCode}, function(responseText, statusText, xhr){
        if(statusText == "success"){
          cerrarGif();
          $().toastmessage('showSuccessToast', "Datos cargados correctamente");
        }
        if(statusText == "error"){
          $().toastmessage('showErrorToast', "No se pudo cargar listado de Categorías de Menú");
          cerrarGif();
        }
      });
  }

function gestionRegistro(aObject){
  switch($(aObject).data('accion')){
    case 'insertarRegistro':
        $("#formActivo")[0].reset();
        $("#idActivo").val("");
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Nuevo Registro");
    break;
    case 'editarRegistro':
        $("#modalFormulario").modal('show');
        $("#tituloModal").text("Editar Registro");
        editarRegistro($(aObject).data('id'));
    break;
    case 'eliminarRegistro':
       
        swal({
          title: 'Desea eliminar?',
          text: "Los datos se perderán!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
          if (result.value) {
            
                $.ajax({
                  type : 'post',
                  url  : "<?php echo site_url('Activos/eliminarRegistro'); ?>",
                  dataType: 'json',
                  data: {
                        idActivo   : $(aObject).data('id'),
                      }
                })  
                .done(function(data){
                  if(data){
                    $().toastmessage('showSuccessToast', "Registro eliminado");
                  }else{
                    $().toastmessage('showErrorToast', "No se pudo eliminar la información (registros enlazados)");
                  }
                })
                .fail(function(){
                  $().toastmessage('showErrorToast', "Error: No se pudo eliminar la información");
                })   
                .always(function(){
                  //$("#modalFormulario").modal('hide');
                  cerrarGif();
                  listarDatos();
                });

          }
        })

    break;
    case 'cancelarRegistro':
      swal({
        title: "Desea cancelar?",
        text: "Los datos se perderán!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, cancelar!",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
      },
      function(){
          swal("Cancelado!", "Se ha cancelado la Inscripción.", "success");
          $(aObject).text('Inscribir')
          $(aObject).removeClass("btn-danger").addClass("btn-info");
          $(aObject).data('accion', 'insertarRegistro');
        listarDatos();
      });       
    break;
  }
}

$(document).ready(function() {

 $('#formActivo').validate({
  rules: {
   nombreActivo: {
    required: true
   },
   descripcionActivo: {
    required: true
   }
  },
  messages: {
   nombreActivo: {
    required: "Ingrese un Activo"
   },
   descripcionActivo: {
    required: "Se requiere una descripción"
   }
  },
  highlight: function(element) {
   $(element).closest('.row').removeClass('success').addClass('error');
  },
  success: function(element) {
  // element.text('OK!').addClass('valid').closest('.row').removeClass('error').addClass('success');
   element.addClass('valid').closest('.row').removeClass('error').addClass('success');
  },
  submitHandler: function(form) {
   // do other stuff for a valid form
   $.post("<?php echo site_url('Activos/gestionRegistro')?>", $("#formActivo").serialize())
     .done(function(data){
                         if(data){
                                data = data.replace('"','');
                                var row = data.split('|');
                                switch(row[0]){
                                    case 'i':
                                      //$('#idParaleloMateriaProfesor').val(row[1]);   
                                      //swal("Exitoso!", "Registro creado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Registro creado exitosamente");
                                    break;
                                    case 'e':
                                      //swal("Exitoso!", "Editado exitosamente", "success"); 
                                      $().toastmessage('showSuccessToast', "Editado exitosamente");
                                      //$("#formActivo")[0].reset();
                                    break;
                                }
                           //listarDatos();     
                           }else{
                                //swal("Información!", "No se pudo procesar la información", "info"); 
                                $().toastmessage('showErrorToast', "No se pudo procesar la información");
                           }
     })
     .fail(function(err){
        //swal("Información!", "Error: No se pudo procesar la información", "warning"); 
        $().toastmessage('showErrorToast', "Error: No se pudo procesar la información");
     })
     .always(function(){
      listarDatos();
       $("#modalFormulario").modal('hide');
       $("#formActivo").find('.error').removeClass("error");
       $("#formActivo").find('.success').removeClass("success");
       $("#formActivo").find('.valid').removeClass("valid");
     });
  }
 });
}); // end document.ready

function editarRegistro(aId){
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Activos/buscarActivoPorID'); ?>",
      dataType: 'json',
      data: {
        idActivo   : aId,
      },
    }).done( function(data) {
      $(data).each(function(i, v){
        $("#idActivo").val(v.idActivo);
        $("#idTipoActivo").val(v.idTipoActivo);
        $("#nombreActivo").val(v.nombreActivo);
        $("#descripcionActivo").val(v.descripcionActivo);
      });              
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      //alert( 'Always' );
    });
}

function vulnerabilidadActivo(aObject){
  $("#modalVulneravilidad").modal('show');
  var idActivo = $(aObject).data('id');
  $("#idVulnerabilidadActivo").val(idActivo);
  buscarVulnerabilidadesDisponibles(idActivo);
  buscarVulnerabilidadAsociadoActivo(idActivo);
}

function buscarVulnerabilidadesDisponibles(idActivo){
  cargarGif();
  //var idActivo = $("#idActivo").val();
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Activos/buscarVulnerabilidadesDisponiblesParaActivo'); ?>",
      dataType: 'json',
      data: {
        idActivo   : idActivo,
      },
    }).done( function(data) {
      $('#idVulnerabilidad').find('option').remove();
      $(data).each(function(i, v){
        $('#idVulnerabilidad').append('<option value="'+ v.idVulnerabilidad +'">' + v.nombreVulnerabilidad+ '</option');
      });
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      cerrarGif();
    });  
}

function buscarVulnerabilidadAsociadoActivo(idActivo){
  //var idPerfil = $("#idPerfil").val();
  var urlCode = "<?php echo $urlCode; ?>";
  //cargarGif();
  $("#listaActivoVulnerabilidad").load("<?php echo site_url('Activos/listaActivoVulnerabilidad'); ?>",{idActivo, urlCode}, function(responseText, statusText, xhr){
    if(statusText == "success"){
      cerrarGif();
    }
    if(statusText == "error"){
    swal("Información!", "No se pudo cargar la información", "info"); 
      //cerrarGif();
    }
  });
}


function agregarRegistro(aObject){
  var idVulnerabilidad = $("#idVulnerabilidad").val();
  if(idVulnerabilidad){
    cargarGif();
    var idActivo = $("#idVulnerabilidadActivo").val();
    $.ajax({
      type : 'post',
      url  : "<?php echo site_url('Activos/agregarRegistroVulnerabilidad'); ?>",
      dataType: 'json',
      data: {
        idActivo : idActivo,
        idVulnerabilidad   : idVulnerabilidad,
      },
    }).done( function(data) {
        if(data){

              $().toastmessage('showSuccessToast', "Registro creado exitosamente");

        }else{
          $().toastmessage('showErrorToast', "No se pudo procesar la información");
        }
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      buscarVulnerabilidadesDisponibles(idActivo);
      buscarVulnerabilidadAsociadoActivo(idActivo);
      cerrarGif();
    });  
  }
}


function eliminarRegistro(aObject){
  
  var idActivo = $("#idVulnerabilidadActivo").val();

  swal({
    title: 'Desea eliminar?',
    text: "Los datos se perderán!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.value) {
         $.ajax({
        type : 'post',
        url  : "<?php echo site_url('Activos/eliminarRegistroVulnerabilidad'); ?>",
        dataType: 'json',
        data: {
          idActivo : idActivo,
          idVulnerabilidad : $(aObject).data("idvulnerabilidad")
        },
      }).done( function(data) {
        if(data){
          $().toastmessage('showSuccessToast', "Registro eliminado");
        }else{
          $().toastmessage('showErrorToast', "No se pudo eliminar la información (registros enlazados)");
        }
    }).fail( function() {
      swal("Información!", "No se pudo cargar la información", "warning"); 
    }).always( function() {
      buscarVulnerabilidadesDisponibles(idActivo);
      buscarVulnerabilidadAsociadoActivo(idActivo);
      cerrarGif();
    }); 
    }
  }) 
}

</script> 