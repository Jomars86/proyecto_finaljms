<br>
<?php if($listaVulnerabilidad){ ?>
            <?php $i=1; ?>
            <?php foreach ($listaVulnerabilidad as $lt) { ?>
              <div class="`col-sm-12">
                  <div class="col-sm-1">
                      <?php echo $i; $i++; ?>
                  </div>
                  <div class="col-sm-9">
                      <?php echo $lt->codigoVulnerabilidad." ".$lt->nombreVulnerabilidad; ?>
                  </div>
                  <div class="col-sm-2">
                      <button type="button" class="btn btn-danger btn-sm" onclick="eliminarRegistro(this);" data-toggle="tooltip" data-placement="left" title="Eliminar Registro" data-accion="eliminarRegistro" data-idvulnerabilidad="<?php echo $lt->idVulnerabilidad; ?>" ><i class="fa fa-trash-o"></i>
                  </div>
              </div>
            <?php } ?>
<?php }else{ ?>
  <br>
  <div class="alert alert-danger alert-mg-b" role="alert">
    <b>No se encontraton datos</b>
  </div>
<?php } ?>

    </div>
</div>
