<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Activo extends CI_Model{


		public function buscarActivo(){
			$sql = "SELECT activo.id as idActivo, activo.tipo_activo_id as idTipoActivo, tipo_activo.nombre as nombreTipoActivo, activo.nombre as nombreActivo, activo.descripcion as descripcionActivo FROM activo, tipo_activo WHERE tipo_activo.id=activo.tipo_activo_id";
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarActivo($data){
			$this->db->insert('activo', $data);
			return $this->db->insert_id();			
		}


		public function editarActivo($idActivo, $data){
			$this->db->where('id', $idActivo);
			$this->db->update('activo', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarActivo($idActivo){
			$this->db->where('id', $idActivo);
			$this->db->delete('activo');
			return $this->db->affected_rows();			
		}

		public function buscarActivoPorID($idActivo){
			$sql = "SELECT id as idActivo, tipo_activo_id as idTipoActivo, nombre as nombreActivo, descripcion as descripcionActivo FROM activo WHERE id=".$idActivo;
			$result = $this->db->query($sql);

			if($result->num_rows()>0){
				return $result->row();
			}
			else{
				return false;
			}

		}

		// Activos Vulnerabilidades
		public function buscarVulnerabilidadesDisponiblesParaActivo($idActivo){
			$sql = "SELECT vulnerabilidad.id as idVulnerabilidad, vulnerabilidad.nombre as nombreVulnerabilidad FROM vulnerabilidad WHERE vulnerabilidad.id NOT IN (SELECT activo_vulnerabilidad.vulnerabilidad_id FROM activo_vulnerabilidad WHERE activo_vulnerabilidad.activo_id = ".$idActivo." ) GROUP BY vulnerabilidad.id ORDER BY vulnerabilidad.nombre";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}
		}	

		public function buscarVulnerabilidadesDeActivo($idActivo){
			$sql = "SELECT vulnerabilidad.id AS idVulnerabilidad, vulnerabilidad.codigo AS codigoVulnerabilidad, vulnerabilidad.nombre AS nombreVulnerabilidad FROM vulnerabilidad INNER JOIN activo_vulnerabilidad ON activo_vulnerabilidad.vulnerabilidad_id = vulnerabilidad.id WHERE activo_vulnerabilidad.activo_id = ".$idActivo." GROUP BY vulnerabilidad.id ORDER BY nombreVulnerabilidad ASC";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}
		}

		public function insertarActivoVulnerabilidad($data){
			$this->db->insert('activo_vulnerabilidad', $data);
			return $this->db->affected_rows();			
		}

		public function eliminarActivoVulnerabilidad($idActivo, $idVulnerabilidad){
			$this->db->where('activo_id', $idActivo);
			$this->db->where('vulnerabilidad_id', $idVulnerabilidad);
			$this->db->delete('activo_vulnerabilidad');
			return $this->db->affected_rows();				
		}


	//Estadisticas
	public function buscarActivoVulnerabilidadAmenaza(){
			$sql = "SELECT activo.id AS idActivo, tipo_activo.nombre AS tipoActivo, activo.nombre AS nombreActivo, activo.descripcion AS descripcionActivo, vulnerabilidad.codigo AS codigoVulnerabilidad, vulnerabilidad.nombre AS nombreVulnerabilidad, amenaza.codigo AS codigoAmenaza, amenaza.nombre AS nombreAmenaza, amenaza.integridad AS integridadAmenaza, amenaza.confidencialidad AS confidencialidadAmenaza, amenaza.disponibilidad AS disponibilidadAmenaza, amenaza.impacto AS impactoAmenaza FROM activo INNER JOIN activo_vulnerabilidad ON activo_vulnerabilidad.activo_id = activo.id INNER JOIN vulnerabilidad ON activo_vulnerabilidad.vulnerabilidad_id = vulnerabilidad.id INNER JOIN vulnerabilidad_amenaza ON vulnerabilidad_amenaza.vulnerabilidad_id = vulnerabilidad.id INNER JOIN amenaza ON vulnerabilidad_amenaza.amenaza_id = amenaza.id INNER JOIN tipo_activo ON activo.tipo_activo_id = tipo_activo.id ORDER BY nombreActivo ASC, nombreVulnerabilidad ASC, nombreAmenaza ASC";

			$result = $this->db->query($sql);
			if($result->num_rows() > 0){
				return $result->result();
			}
			else{
				return false;
			}
	}

}

?>