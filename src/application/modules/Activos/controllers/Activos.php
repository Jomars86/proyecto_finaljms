<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activos extends MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('CategoriasMenus/CategoriaMenu');
		$this->load->model('PerfilesMenus/PerfilMenu');
		$this->load->model('Menus/Menu');
		$this->load->model('Activos/Activo');
		$this->load->model('TiposActivos/TipoActivo');

		$this->load->helper('session_helper');	
		$this->load->helper('utilidades_helper');	
	}
	
	public function index($idMenu){
	  $urlCode = $idMenu;
	  $idMenu = desencriptar($idMenu);
	  if(verficarAcceso($idMenu)){
	    $dataSession = verificarPrivilegios($idMenu);
		//$idEmpresa = $this->session->userdata("idEmpresa");
	    $data['status'] = $dataSession->status;
		$data['menuNombre'] = $dataSession->nombreMenu;
		$data['codigoCategoriaMenu'] = $dataSession->codigoCategoriaMenu;
		$data['codigoMenu'] = $dataSession->codigoMenu;
		$data['urlCode'] = $urlCode;
		//Vista
		$data['tipoActivo'] = $this->TipoActivo->buscarTipoActivo();
		$data['view'] = 'Activos/index';
		$data['output'] = '';
		$this->load->view('Modulos/main_administrativo',$data);

	  }
	  else{ 
	  	redirect('Login/Login');
	  }
	}
	
	public function lista(){
		$data['lista'] = $this->Activo->buscarActivo();
		$urlCode = $this->input->post("urlCode");
		$idMenu = desencriptar($urlCode);
		$dataSession = verificarPrivilegios($idMenu);
		$data['status'] = $dataSession->status;
		$this->load->view('Activos/lista',$data);
	}

	public function gestionRegistro(){
		$idActivo = $this->input->post("idActivo");
		$idTipoActivo = $this->input->post("idTipoActivo");
		$nombreActivo = $this->input->post("nombreActivo");
		$descripcionActivo = $this->input->post("descripcionActivo");


		$data = array(
					"tipo_activo_id" => $idTipoActivo,
					"nombre" => $nombreActivo,
					"descripcion" => $descripcionActivo
				);

		if($idActivo > 0){
			echo json_encode('e|'.$this->Activo->editarActivo($idActivo, $data));
		}
		else{
			echo json_encode('i|'.$this->Activo->insertarActivo($data));
		}
	}

	public function buscarActivoPorID(){
		$idActivo = $this->input->post("idActivo");
		$data = $this->Activo->buscarActivoPorID($idActivo);
		print_r(json_encode($data));		
	}

	public function eliminarRegistro(){
		$idActivo = $this->input->post("idActivo");
			
			$resultado = $this->Activo->eliminarActivo($idActivo);
			if($resultado){
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
	}

	public function buscarVulnerabilidadesDisponiblesParaActivo(){
		$idActivo = $this->input->post("idActivo");
		$data = $this->Activo->buscarVulnerabilidadesDisponiblesParaActivo($idActivo);
		print_r(json_encode($data));
	}

	public function listaActivoVulnerabilidad(){
		$idActivo = $this->input->post("idActivo");
		$data['listaVulnerabilidad'] = $this->Activo->buscarVulnerabilidadesDeActivo($idActivo);
		$this->load->view('Activos/listaActivoVulnerabilidad',$data);
	}

	public function agregarRegistroVulnerabilidad(){
		$idActivo = $this->input->post("idActivo");
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		$data =  array("activo_id" => $idActivo,
					   "vulnerabilidad_id" => $idVulnerabilidad
					  );
		echo json_encode($this->Activo->insertarActivoVulnerabilidad($data));
	}

	public function eliminarRegistroVulnerabilidad(){
		$idActivo = $this->input->post("idActivo");
		$idVulnerabilidad = $this->input->post("idVulnerabilidad");
		echo json_encode($this->Activo->eliminarActivoVulnerabilidad($idActivo, $idVulnerabilidad));
	}


}