<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modulos extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index(){
		$this->load->view('Login/index');
	}

	public function escritorio(){
		$data['view'] = 'Modulos/escritorio';
		$data['output'] = '';
		$this->load->view('Modulos/main_administrativo',$data);
	}
}